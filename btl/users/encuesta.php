<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../estilos.css" rel="stylesheet" type="text/css">
<script src="SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
<script language="JavaScript" type="text/JavaScript">

</script>
<link rel="stylesheet" type="text/css" href="tcal.css" />
<script type="text/javascript" src="../tcal.js"></script>
<link href="SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Tabla {
	font-family: "Century Gothic";
	font-size: 12px;
}

.Tabla2 {
	font-family: "Century Gothic"; 
	font-size: 9px;
}

.titulos {
	color: #FFF;
	background-color:#D00;
	font-size: 14px;
	font-weight: bold;
}

.preguntas {
	color: #000;
	background-color:#CCC;
	font-size: 11px;
	
}
-->
</style>

<style type="text/css">
</style>

<script language="JavaScript"> 
function pregunta(){ 
    if (confirm('¿Estas seguro de enviar este formulario?')){ 
       document.tuformulario.submit() 
    } 
} 
</script>

</head>

<body>
<?php


include('include_sup.php');

$string_intro = getenv("QUERY_STRING"); 
parse_str($string_intro);

$Nick ;

?>
<form id="form1" name="tuformulario" method="post" action="inser_encuesta.php?Nick=<?php echo $Nick; ?>"> 
<table width="1076" border="1" rules="rows" class="Tabla">
  <tr bgcolor="#D00" class="titulos">
    <td width="123" rowspan="2" class="titulos" align="center">COCA-COLA FEMSA</td>
    <td height="25" colspan="4" align="center">CONVENIO XXXXXX</td>
    <td colspan="6" >VERSIÓN No. 1</td>
  </tr>
  <tr>
    <td height="25" colspan="4" bgcolor="#D00" class="titulos" align="center">FORMATO CENSO SOCIAL A CONCESIONARIOS Y PERSONAL DE APOYO</td>
    <td colspan="6" bgcolor="#D00" class="titulos">Pág. 1 de 2</td>
  </tr>
  <tr bgcolor="#D00" class="titulos">
    <td colspan="11">I.  INFORMACIÓN PERSONAL</td>
  </tr>
  <tr>
    <td class="preguntas">1. NOMBRES: </td>
    <td width="134"><input name="NOMBRES" type="text" class="campos_formulario" id="NOMBRES" size="20" /></td>
    <td width="142" class="preguntas">2. APELLIDOS: </td>
    <td width="275"><input name="APELLIDOS" type="text" class="campos_formulario" id="APELLIDOS" size="30" /></td>
    <td width="132" class="preguntas">3. TIPO DOCUMENTO: </td>
    <td colspan="6">
    <select name="TIPO_DOC" class="Tabla" id="TIPO_DOC">        
        <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
        <option>C.C.</option> 
        <option>C.E.</option>
        <option>RUT</option> 		
      </select>
    </td>
  </tr>
  <tr>
    <td class="preguntas">4. No. </td>
    <td><input name="NUM_DOC" type="text" class="campos_formulario" id="NUM_DOC" size="20" /></td>
    <td class="preguntas">5. FECHA DE NACIMIENTO: </td>
    <td>
   <!-- <input name="FECHA_INI" type="text" class="tcal" id="FECHA_INI" value="" size="10" readonly="readonly" /> --
      A&Ntilde;O: -->
      <select name="ANO_NACI" id="ANO_NACI">
      <option></option>
        <?php
        for($i=date('o'); $i>=1910; $i--){
            if ($i == date('o'))
                echo '<option value="'.$i.'">'.$i.'</option>';
            else
                echo '<option value="'.$i.'">'.$i.'</option>';
        }
        ?>
      </select>
      
        
       <!-- MES: -->
        <select name="MES_NACI" id="MES_NACI">
        <option></option>
        <?php
	for($m = 1; $m<=12; $m++)
	{
		if($m<10)
			$me = "0" . $m;
		else
			$me = $m;
			
		switch($me)
		{
			case "01": $mes = "Enero"; break;
			case "02": $mes = "Febrero"; break;
			case "03": $mes = "Marzo"; break;
			case "04": $mes = "Abril"; break;
			case "05": $mes = "Mayo"; break;
			case "06": $mes = "Junio"; break;
			case "07": $mes = "Julio"; break;
			case "08": $mes = "Agosto"; break;
			case "09": $mes = "Septiembre"; break;
			case "10": $mes = "Octubre"; break;
			case "11": $mes = "Noviembre"; break;
			case "12": $mes = "Diciembre"; break;			
		}
		echo "<option value='$me'>$mes</option>";
	}
		
/*		
        for ($i=1; $i<=12; $i++) {
            if ($i == date('mm'))
                echo '<option value="'.$i.'" selected>'.$i.'</option>';
            else
                echo '<option value="'.$i.'">'.$i.'</option>';
        }
  */      ?>
      </select>
        
        
     <!-- DIA: -->
      <select name="DIA_NACI" id="DIA_NACI">
      <option></option>
        <?php
	for($d=1;$d<=31;$d++)  
	{
		if($d<10) 
			$dd = "0" . $d; 
		else
			$dd = $d; 
		echo "<option value='$dd'>$dd</option>";
	}	
		
/*		
        for ($i=1; $i<=31; $i++) {
            if ($i == date('j'))
                echo '<option value="'.$i.'" selected>'.$i.'</option>';
            else
                echo '<option value="'.$i.'">'.$i.'</option>';
        }
  */      ?>
      </select></td>
    <td class="preguntas">6. LUGAR DE NACIMIENTO: </td>
    <td colspan="6"><input name="LUGAR_NACI" type="text" class="campos_formulario" id="LUGAR_NACI" size="30" /></td>
  </tr>
  <tr>
    <td class="preguntas">7. GENERO: </td>
    <td><select name="GENERO" class="Tabla" id="GENERO">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>MASC</option>
      <option>FEM</option>
    </select></td>
    <td class="preguntas">8. ESTADO CIVIL : </td>
    <td><select name="ESTADO_CIVIL" class="Tabla" id="ESTADO_CIVIL">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>SOLTERO</option>
      <option>CASADO</option>
      <option>VIUDO</option>
      <option>UNION LIBRE</option>
      <option>OTRO</option>
    </select></td>
    <td class="preguntas">9. CUAL? </td>
    <td colspan="6"><input name="OTRO_ESTADO" type="text" class="campos_formulario" id="OTRO_ESTADO" size="30" /></td>
  </tr>
  <tr>
    <td class="preguntas">10. DIRECCIÓN: </td>
    <td colspan="3"><input name="DIRECCION" type="text" class="campos_formulario" id="DIRECCION" size="60" /></td>
    <td class="preguntas">11. BARRIO: </td>
    <td colspan="6"><input name="BARRIO" type="text" class="campos_formulario" id="BARRIO" size="30" /></td>
  </tr>
  <tr>
    <td class="preguntas">12. CIUDAD:</td>
    <td><input name="CIUDAD" type="text" class="campos_formulario" id="CIUDAD" size="20" /></td>
    <td class="preguntas">13. DEPARTAMENTO: </td>
    <td><input name="DEPARTAMENTO" type="text" class="campos_formulario" id="DEPARTAMENTO" size="30" /></td>
    <td class="preguntas">14. TELEFONO MOVIL: </td>
    <td colspan="6"><input name="TEL_CELULAR" type="text" class="campos_formulario" id="TEL_CELULAR" size="30" /></td>
  </tr>
  <tr>
    <td class="preguntas">15. TELÉFONO FIJO: </td>
    <td><input name="TEL_FIJO" type="text" class="campos_formulario" id="TEL_FIJO" size="20" /></td>
    <td class="preguntas">16. CORREO ELECTRONICO: </td>
    <td><input name="CORREO" type="text" class="campos_formulario" id="CORREO" size="30" /></td>
    <td class="preguntas">17. ESTRATO:</td>
    <td colspan="6"><select name="ESTRATO" class="Tabla" id="ESTRATO">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>1</option>
      <option>2</option>
      <option>3</option>
      <option>4</option>
      <option>5</option>
      <option>6</option>
    </select></td>
  </tr>
  <tr bgcolor="#CCCCCC" class="titulos">
    <td colspan="11" bgcolor="#D00">II. RELACIÓN LABORAL</td>
  </tr>
  <tr>
    <td class="preguntas">18. OFICIO - TIPO DE PLANILLA </td>
    <td><select name="TIPO_PLANILLA" class="Tabla" id="TIPO_PLANILLA">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>CONCESIONADO</option>
      <option>AYUDANTE</option>
    </select></td>
    <td class="preguntas">19. ANTES DE SER CONSESIONARIO / AYUDANTE TENIA ALGUN VINCULO CON COCA-COLA FEMSA</td>
    <td><select name="PREG_19" class="Tabla" id="PREG_19">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>SI</option>
      <option>NO</option>
    </select></td>
    <td class="preguntas">20. POR CUANTO TIEMPO MANTUVO EL VINCULO ?</td>
    <td colspan="6"><input name="PREG_20" type="text" class="campos_formulario" id="PREG_20" size="30" /></td>
  </tr>
  <tr>
    <td class="preguntas">21. EN QUE EPS SE ENCUENTRA ? </td>
    <td><input name="PREG_21" type="text" class="campos_formulario" id="PREG_21" size="20" /></td>
    <td class="preguntas">22. NOMBRE DE FONDO DE PENSIONES: </td>
    <td><input name="PREG_22" type="text" class="campos_formulario" id="PREG_22" size="30" /></td>
    <td class="preguntas">23. HACE CUANTO ESTA AFILIADO A PENSION</td>
    <td colspan="6"><select name="PREG_23" class="Tabla" id="PREG_23">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>1-6 MESES</option>
      <option>6-12 MESES</option>
      <option>1-2 ANOS</option>
      <option>2-5 ANOS</option>
      <option>5-10 ANOS</option>
      <option>> A 10 ANOS</option>
    </select></td>
  </tr>
  <tr>
    <td class="preguntas">24. CESANTIAS:</td>
    <td><select name="PREG_24" class="Tabla" id="PREG_24">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>SI</option>
      <option>NO</option>
    </select></td>
    <td class="preguntas">26. CUAL?</td>
    <td><input name="PREG_26" type="text" class="campos_formulario" id="PREG_26" size="30" /></td>
    <td class="preguntas">28. A QUE ARL SE ENCUENTRA AFILIADO?</td>
    <td colspan="6"><input name="PREG_28" type="text" class="campos_formulario" id="PREG_28" size="30" /></td>
  </tr>
  <tr>
    <td class="preguntas">25. CAJA DE COMPENSACIÓN FAMILIAR: </td>
    <td><select name="PREG_25" class="Tabla" id="PREG_25">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>SI</option>
      <option>NO</option>
    </select></td>
    <td class="preguntas">27. CUAL?</td>
    <td><input name="PREG_27" type="text" class="campos_formulario" id="PREG_27" size="30" /></td>
    <td class="preguntas">29. NIVEL ACADEMICO</td>
    <td colspan="6"><select name="PREG_29" class="Tabla" id="PREG_29">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>PRIMARIA</option>
      <option>BACHILLERATO</option>
      <option>TECNICO</option>
      <option>TECNOLOGICO</option>
      <option>PROFECIONAL</option>
      <option>POSGRADO</option>
    </select></td>
  </tr>
  <tr>
    <td class="preguntas">30. SI DESEARA CONTINUAR CON SU EDUCACIÓN, QUE AREA DEL CONOCIMIENTO LE INTERESARÍA?</td>
    <td><select name="PREG_30" class="Tabla" id="PREG_30">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>INGENIERIA</option>
      <option>SALUD</option>
      <option>DERECHO</option>
      <option>FINANZAS</option>
      <option>ARTE</option>
      <option>HUMANIDADES</option>
      <option>DEPORTES</option>
      <option>CIENCIAS</option>
    </select></td>
    <td class="preguntas">31. QUE LE GUSTARIA REFORZAR DE SU LABOR DE EMPLEADOR / EMPLEADO ?</td>
    <td><input name="PREG_31" type="text" class="campos_formulario" id="PREG_31" size="30" /></td>
    <td class="preguntas">32. TIEMPO DE VINCUALCIÓN COMO CONCESIONARIO / AYUDANTE</td>
    <td colspan="6"><select name="PREG_32" class="Tabla" id="PREG_32">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>1-6 MESES</option>
      <option>6-12 MESES</option>
      <option>1-2 ANOS</option>
      <option>2-5 ANOS</option>
      <option>5-10 ANOS</option>
      <option> > A 10 ANOS</option>
    </select></td>
  </tr>
  <tr>
    <td class="preguntas">33. INGRESOS PROMEDIO DE SU OFICIO DE CONCESIÓN / AYUDANTE</td>
    <td><select name="PREG_33" class="Tabla" id="PREG_33">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>1 smmlv</option>
      <option>1 a 2 smmlv</option>
      <option>2 a 3 smmlv</option>
      <option>3 a 4 smmlv</option>
      <option>5 O mas smmlv</option>      
    </select></td>
    <td class="preguntas">34. RECIBE DINERO POR OTRA ACTIVIDAD?</td>
    <td><select name="PREG_34" class="Tabla" id="PREG_34">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>NO</option>
      <option>INGRESOS FINANCIEROS</option>
      <option>ARRIENDOS</option>
      <option>SALARIOS ADICIONALES (HONORARIOS)</option>
      <option>PENSIONES</option>
      <option>NEGOCIO PROPIO ALTERNO</option>
      <option>OTROS</option>
    </select></td>
    <td class="preguntas">CUAL ?</td>
    <td colspan="6"><input name="OTROS_PREG_34" type="text" class="campos_formulario" id="OTROS_PREG_34" size="30" /></td>
  </tr>
  <tr bgcolor="#CCCCCC" class="titulos">
    <td colspan="11" bgcolor="#D00">III. PATRIMONIO FAMILIAR</td>
  </tr>
  <tr>
    <td rowspan="2" class="preguntas">TIPO VIVIENDA</td>
    <td rowspan="2"><select name="TIPO_VIVIENDA" class="Tabla" id="TIPO_VIVIENDA">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>CASA</option>
      <option>APARTAMENTO</option>
      <option>HABITACI&Oacute;N</option>
      <option>FINCA</option>
    </select></td>
    <td rowspan="2" class="preguntas">ESTADO VIVIENDA</td>
    <td rowspan="2"><select name="ESTADO_VIVIENDA" class="Tabla" id="ESTADO_VIVIENDA">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>PROPIA</option>
      <option>ARRIENDO</option>
      <option>FAMILIAR</option>
    </select></td>
    <td rowspan="2" class="preguntas">36.. SERVICIOS PUBLICOS CON QUE CUENTA EN SU VIVIENDA</td>
    <td width="43">AGUA</td>
    <td width="34">LUZ</td>
    <td width="19">TEL</td>
    <td width="38">GAS</td>
    <td width="22">INT</td>
    <td width="44">CABLE</td>
  </tr>
  <tr>
    <td>
  <!--  <select name="PREG_36" class="Tabla" id="PREG_36">
      <option>AGUA</option>
      <option>LUZ</option>
      <option>TELEFONO</option>
      <option>GAS</option>
      <option>INTERNET</option>
      <option>TV POR CABLE</option>
    </select> -->
    <select name="AGUA_36" class="Tabla2" id="AGUA_36">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>SI</option>
      <option>NO</option>
    </select></td>
    <td><select name="LUZ_36" class="Tabla2" id="LUZ_36">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>SI</option>
      <option>NO</option>
    </select></td>
    <td><select name="TEL_36" class="Tabla2" id="TEL_36">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>SI</option>
      <option>NO</option>
    </select></td>
    <td><select name="GAS_36" class="Tabla2" id="GAS_36">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>SI</option>
      <option>NO</option>
    </select></td>
    <td><select name="INT_36" class="Tabla2" id="INT_36">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>SI</option>
      <option>NO</option>
    </select></td>
    <td><select name="CABLE_36" class="Tabla2" id="CABLE_36">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>SI</option>
      <option>NO</option>
    </select></td>
  </tr>
  <tr>
    <td class="preguntas">37. CUANTA CON COMPUTADOR ACTUALMENTE? :</td>
    <td><select name="PREG_37" class="Tabla" id="PREG_37">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>SI</option>
      <option>NO</option>
    </select></td>
    <td class="preguntas">38. CUANTOS?: </td>
    <td><input name="PREG_38" type="text" class="campos_formulario" id="PREG_38" size="30" /></td>
    <td class="preguntas">39. CUAL ES EL NIVEL DE INGRESOS DE SU NUCLEO FAMILIAR</td>
    <td colspan="6"><select name="PREG_39" class="Tabla" id="PREG_39">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>1 smmlv</option>
      <option>HASTA 1 MILLON</option>
      <option>HASTA 2 MILLON</option>
      <option>HASTA 3 MILLON</option>
      <option>MAS DE 3 MILLON</option>
    </select></td>
  </tr>
  <tr>
    <td class="preguntas">40. POSEE CUENTA CON ENTIDAD FINANCIERA</td>
    <td><select name="PREG_40" class="Tabla" id="PREG_40">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>SI</option>
      <option>NO</option>
    </select></td>
    <td class="preguntas">TIPO DE CUENTA</td>
    <td><select name="TIPO_CUENTA" class="Tabla" id="TIPO_CUENTA">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>CUENTA DE AHORROS</option>
      <option>CUENTA CORRIENTE</option>
      <option>AHORRO PROGRAMADO</option>
      <option>AHORRO COOPERATIVO</option>
      </select></td>
    <td class="preguntas">NOMBRE DE LA ENTIDAD FINANCIERA O COOPERATIVA</td>
    <td colspan="6"><input name="NOMBRE_ENTIDAD_FIN" type="text" class="campos_formulario" id="NOMBRE_ENTIDAD_FIN" size="30" /></td>
  </tr>
  <tr>
    <td class="preguntas">41. POSEE CREDITO CON ENTIDAD FINANCIERA O COOPERATIVA: </td>
    <td><select name="PREG_41" class="Tabla" id="PREG_41">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>SI</option>
      <option>NO</option>
    </select></td>
    <td class="preguntas">42. POSEE TARJETA DE CREDITO</td>
    <td><select name="PREG_42" class="Tabla" id="PREG_42">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>SI</option>
      <option>NO</option>
    </select></td>
    <td class="preguntas">44.TIENE LICENCIA DE CONDUCCIÓN?</td>
    <td colspan="6"><select name="PREG_44" class="Tabla" id="PREG_44">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>SI</option>
      <option>NO</option>
    </select></td>
  </tr>
  <tr>
    <td class="preguntas">43. CUANTA CON CRÉDITO HIPOTECARIO </td>
    <td><select name="PREG_43" class="Tabla" id="PREG_43">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>SI</option>
      <option>NO</option>
    </select></td>
    <td class="preguntas"> CUAL ENTIDAD?</td>
    <td><input name="CUAL_PREG_43" type="text" class="campos_formulario" id="CUAL_PREG_43" size="30" /></td>
    <td class="preguntas">CATEGORIA LICENCIA: </td>
    <td colspan="6"><input name="CATEGORIA_LICENCIA" type="text" class="campos_formulario" id="CATEGORIA_LICENCIA" size="30" /></td>
  </tr>
  <tr bgcolor="#CCCCCC" class="titulos">
    <td colspan="11" bgcolor="#D00">IV.  ACTIVIDADES</td>
  </tr>
  <tr>
    <td class="preguntas">45. TOMA DESCANSOS ANUALES (VACACIONES)</td>
    <td><select name="PREG_45" class="Tabla" id="PREG_45">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>SI</option>
      <option>NO</option>
    </select></td>
    <td class="preguntas">46. CUANTAS VECES EN EL MES REALIZA ACTIVIDAD FISICA DIFERENTE A SU ACTIVIDAD ECONOMICA O LABORAL?</td>
    <td><select name="PREG_46" class="Tabla" id="PREG_46">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>1-2 VECES </option>
      <option>3-4 VECES </option>
      <option>5 O MAS</option>
      <option>NINGUNA</option>
    </select></td>
    <td class="preguntas">47. QUE ACTIVIDADES REALIZA EN ESTOS DESCANSOS</td>
    <td colspan="6"><select name="PREG_47" class="Tabla" id="PREG_47">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>DESCANSO EN CASA</option>
      <option>VIAJE VISITA FAMILIARES</option>
      <option>VIAJE DE TURISMO</option>
      <option>UTILIZA CAJA DE COMPENSACI&Oacute;N</option>
    </select></td>
  </tr>
  <tr>
    <td class="preguntas">48. CUANTAS VECES VISITA AL ODONTOLOGO?</td>
    <td><input name="PREG_48" type="text" class="campos_formulario" id="PREG_48" size="20" /></td>
    <td class="preguntas">&nbsp;</td>
    <td>&nbsp;</td>
    <td class="preguntas">&nbsp;</td>
    <td colspan="6">&nbsp;</td>
  </tr>
  <tr bgcolor="#CCCCCC" class="titulos">
    <td colspan="11" bgcolor="#D00">V. INFORMACIÓN FAMILIAR</td>
  </tr>
  <tr>
    <td rowspan="7" class="preguntas">49.INDIQUE CON QUIEN VIVE Y EL N&Uacute;MERO DE PERSONAS QUE TIENE A CARGO. </td>
    <td class="preguntas">PARENTESCO</td>
    <td>NO. PERSONAS A CARGO</td>
    <td class="preguntas">50. CUANTAS PERSONAS APORTAN DINERO AL HOGAR? </td>
    <td><input name="PREG_50" type="text" class="campos_formulario" id="PREG_50" size="10" /></td>
    <td colspan="6">&nbsp;</td>
  </tr>
  <tr>
    <td class="preguntas">ESPOSA</td>
    <td><input name="PREG_49_ESPOSA" type="text" class="campos_formulario" id="PREG_49_ESPOSA" size="10" /></td>
    <td class="preguntas">51. NOMBRE DEL CONYUGE:</td>
    <td><input name="NOMBRE_CONYUGE" type="text" class="campos_formulario" id="NOMBRE_CONYUGE" size="20" /></td>
    <td colspan="6">&nbsp;</td>
  </tr>
  <tr>
    <td class="preguntas">HIJOS</td>
    <td><input name="PREG_49_HIJOS" type="text" class="campos_formulario" id="PREG_49_HIJOS" size="10" /></td>
    <td class="preguntas">52. TIPO DOCUMENTO: </td>
    <td><select name="TIPO_DOC_CONYUGE" class="Tabla" id="TIPO_DOC_CONYUGE">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>C.C.</option>
      <option>C.E.</option>
      <option>RUT</option>
    </select></td>
    <td colspan="6">No.
      <input name="NO_CONYUGE" type="text" class="campos_formulario" id="NO_CONYUGE" size="20" /></td>
  </tr>
  <tr>
    <td class="preguntas">PADRES</td>
    <td><input name="PREG_49_PADRES" type="text" class="campos_formulario" id="PREG_49_PADRES" size="10" /></td>
    <td class="preguntas">53. FECHA DE NACIMIENTO: </td>
    <td colspan="7">
      <!--  <input name="FECHA_INI2" type="text" class="tcal" id="FECHA_INI2" value="" size="10" readonly="readonly" /> -->
      
      <select name="ANO_NACI_CONYUGE" id="ANO_NACI_CONYUGE">
        <option></option>
        <?php
        for($i=date('o'); $i>=1910; $i--){
            if ($i == date('o'))
                echo '<option value="'.$i.'">'.$i.'</option>';
            else
                echo '<option value="'.$i.'">'.$i.'</option>';
        }
        ?>
      </select>
      
      <select name="MES_NACI_CONYUGE" id="MES_NACI_CONYUGE">
      <option></option>
        <?php
	for($m = 1; $m<=12; $m++)
	{
		if($m<10)
			$me = "0" . $m;
		else
			$me = $m;
			
		switch($me)
		{
			case "01": $mes = "Enero"; break;
			case "02": $mes = "Febrero"; break;
			case "03": $mes = "Marzo"; break;
			case "04": $mes = "Abril"; break;
			case "05": $mes = "Mayo"; break;
			case "06": $mes = "Junio"; break;
			case "07": $mes = "Julio"; break;
			case "08": $mes = "Agosto"; break;
			case "09": $mes = "Septiembre"; break;
			case "10": $mes = "Octubre"; break;
			case "11": $mes = "Noviembre"; break;
			case "12": $mes = "Diciembre"; break;			
		}
		echo "<option value='$me'>$mes</option>";
	}
		
      ?>
      </select>
      
      <select name="DIA_NACI_CONYUGE" id="DIA_NACI_CONYUGE">
      <option></option>
        <?php
	for($d=1;$d<=31;$d++)  
	{
		if($d<10) 
			$dd = "0" . $d; 
		else
			$dd = $d; 
		echo "<option value='$dd'>$dd</option>";
	}	
		
      ?>
      </select>
      
    </td>
  </tr>
  <tr>
    <td class="preguntas">ABUELOS</td>
    <td><input name="PREG_49_ABUELOS" type="text" class="campos_formulario" id="PREG_49_ABUELOS" size="10" /></td>
    <td class="preguntas">54. NIVEL ACADEMICO CONYUGE</td>
    <td colspan="7"><select name="PREG_54" class="Tabla" id="PREG_54">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>PRIMARIA</option>
      <option>BACHILLERATO</option>
      <option>TECNICO</option>
      <option>TECNOLOGICO</option>
      <option>PROFECIONAL</option>
      <option>POSGRADO</option>
    </select></td>
  </tr>
  <tr>
    <td class="preguntas">OTRO</td>
    <td><input name="PREG_49_OTROS" type="text" class="campos_formulario" id="PREG_49_OTROS" size="10" /></td>
    <td class="preguntas">55. OCUPACIÓN CONYUGUE:</td>
    <td colspan="7"><select name="PREG_55" class="Tabla" id="PREG_55">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>EMPLEADO</option>
      <option>INDEPENDIENTE</option>
      <option>EMPLEADOR</option>
      <option>SIN EMPLEO</option>
    </select></td>
  </tr>
  <tr>
    <td class="preguntas">CUAL</td>
    <td><input name="PREG_49_CUALES" type="text" class="campos_formulario" id="PREG_49_CUALES" size="20" /></td>
    <td class="preguntas">57. ALGUN MIEMBRO DE LA FAMILIA SE ENCUENTRA EN CONDICIÓN DE DISCAPACIDAD?</td>
    <td colspan="7"><select name="PREG_57" class="Tabla" id="PREG_57">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option></option>
      <option>VISUAL</option>
      <option>FISICA</option>
      <option>MENTAL</option>
      <option>AUDITIVA</option>
    </select></td>
  </tr>
  <tr bgcolor="#D00" class="titulos">
    <td> 56. INFORMACION HIJOS</td>
    <td>NOMBRES</td>
    <td>GENERO</td>
    <td>FECHA NACIMIENTO</td>
    <td>NIVEL ACADEMICO</td>
    <td colspan="6">OCUPACI&Oacute;N</td>
  </tr>
  <tr>
    <td class="preguntas">1</td>
    <td><input name="NOMBRE_HIJO_1" type="text" class="campos_formulario" id="NOMBRE_HIJO_1" size="20" /></td>
    <td><select name="GENERO_HIJO_1" class="Tabla" id="GENERO_HIJO_1">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option></option>  
      <option>MASC</option>
      <option>FEM</option>
    </select></td>
    <td>
  <!--  <input name="FECHA_INI3" type="text" class="tcal" id="FECHA_INI3" value="" size="10" readonly="readonly" /> -->
    
    <select name="ANO_NACI_HIJO_1" id="ANO_NACI_HIJO_1">
    <option></option>
        <?php
        for($i=date('o'); $i>=1910; $i--){
            if ($i == date('o'))
                echo '<option value="'.$i.'">'.$i.'</option>';
            else
                echo '<option value="'.$i.'">'.$i.'</option>';
        }
        ?>
 </select>
                    
 <select name="MES_NACI_HIJO_1" id="MES_NACI_HIJO_1">
 <option></option>
        <?php
	for($m = 1; $m<=12; $m++)
	{
		if($m<10)
			$me = "0" . $m;
		else
			$me = $m;
			
		switch($me)
		{
			case "01": $mes = "Enero"; break;
			case "02": $mes = "Febrero"; break;
			case "03": $mes = "Marzo"; break;
			case "04": $mes = "Abril"; break;
			case "05": $mes = "Mayo"; break;
			case "06": $mes = "Junio"; break;
			case "07": $mes = "Julio"; break;
			case "08": $mes = "Agosto"; break;
			case "09": $mes = "Septiembre"; break;
			case "10": $mes = "Octubre"; break;
			case "11": $mes = "Noviembre"; break;
			case "12": $mes = "Diciembre"; break;			
		}
		echo "<option value='$me'>$mes</option>";
	}
		
      ?>
 </select>
                     
 <select name="DIA_NACI_HIJO_1" id="DIA_NACI_HIJO_1">
 <option></option>
 <option></option>
        <?php
	for($d=1;$d<=31;$d++)  
	{
		if($d<10) 
			$dd = "0" . $d; 
		else
			$dd = $d; 
		echo "<option value='$dd'>$dd</option>";
	}	
		
      ?>
 </select>
    </td>
    <td><select name="N_ACADEMICO_HIJO1" class="Tabla" id="N_ACADEMICO_HIJO1">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option></option>  
      <option>PRIMARIA</option>
      <option>BACHILLERATO</option>
      <option>TECNICO</option>
      <option>TECNOLOGICO</option>
      <option>PROFECIONAL</option>
      <option>POSGRADO</option>
    </select></td>
    <td colspan="6"><select name="OCUPACION_HIJO1" class="Tabla" id="OCUPACION_HIJO1">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option></option>  
      <option>ESTUDIANTE</option>
      <option>EMPLEADO</option>
      <option>INDEPENDIENTE</option>
      <option>DESEMPLEADO</option>
    </select></td>
  </tr>
  <tr>
    <td class="preguntas">2</td>
    <td><input name="NOMBRE_HIJO_2" type="text" class="campos_formulario" id="NOMBRE_HIJO_2" size="20" /></td>
    <td><select name="GENERO_HIJO_2" class="Tabla" id="GENERO_HIJO_2">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option></option>  
      <option>MASC</option>
      <option>FEM</option>
    </select></td>
    <td>
    <!--<input name="FECHA_INI4" type="text" class="tcal" id="FECHA_INI4" value="" size="10" readonly="readonly" /> -->
    
      <select name="ANO_NACI_HIJO_2" id="ANO_NACI_HIJO_2">
      <option></option>
        <?php
        for($i=date('o'); $i>=1910; $i--){
            if ($i == date('o'))
                echo '<option value="'.$i.'">'.$i.'</option>';
            else
                echo '<option value="'.$i.'">'.$i.'</option>';
        }
        ?>
      </select>
      <select name="MES_NACI_HIJO_2" id="MES_NACI_HIJO_2">
      <option></option>
        <?php
	for($m = 1; $m<=12; $m++)
	{
		if($m<10)
			$me = "0" . $m;
		else
			$me = $m;
			
		switch($me)
		{
			case "01": $mes = "Enero"; break;
			case "02": $mes = "Febrero"; break;
			case "03": $mes = "Marzo"; break;
			case "04": $mes = "Abril"; break;
			case "05": $mes = "Mayo"; break;
			case "06": $mes = "Junio"; break;
			case "07": $mes = "Julio"; break;
			case "08": $mes = "Agosto"; break;
			case "09": $mes = "Septiembre"; break;
			case "10": $mes = "Octubre"; break;
			case "11": $mes = "Noviembre"; break;
			case "12": $mes = "Diciembre"; break;			
		}
		echo "<option value='$me'>$mes</option>";
	}
		
      ?>
      </select>
      <select name="DIA_NACI_HIJO_2" id="DIA_NACI_HIJO_2">
      <option></option>
        <?php
	for($d=1;$d<=31;$d++)  
	{
		if($d<10) 
			$dd = "0" . $d; 
		else
			$dd = $d; 
		echo "<option value='$dd'>$dd</option>";
	}	
		
      ?>
    </select></td>
    <td><select name="N_ACADEMICO_HIJO2" class="Tabla" id="N_ACADEMICO_HIJO2">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option></option>  
      <option>PRIMARIA</option>
      <option>BACHILLERATO</option>
      <option>TECNICO</option>
      <option>TECNOLOGICO</option>
      <option>PROFECIONAL</option>
      <option>POSGRADO</option>
    </select></td>
    <td colspan="6"><select name="OCUPACION_HIJO2" class="Tabla" id="OCUPACION_HIJO2">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option></option>  
      <option>ESTUDIANTE</option>
      <option>EMPLEADO</option>
      <option>INDEPENDIENTE</option>
      <option>DESEMPLEADO</option>
    </select></td>
  </tr>
  <tr>
    <td class="preguntas">3</td>
    <td><input name="NOMBRE_HIJO_3" type="text" class="campos_formulario" id="NOMBRE_HIJO_3" size="20" /></td>
    <td><select name="GENERO_HIJO_3" class="Tabla" id="GENERO_HIJO_3">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option></option>  
      <option>MASC</option>
      <option>FEM</option>
    </select></td>
    <td>
    <!-- <input name="FECHA_INI5" type="text" class="tcal" id="FECHA_INI5" value="" size="10" readonly="readonly" /> -->
    
      <select name="ANO_NACI_HIJO_3" id="ANO_NACI_HIJO_3">
      <option></option>
        <?php
        for($i=date('o'); $i>=1910; $i--){
            if ($i == date('o'))
                echo '<option value="'.$i.'">'.$i.'</option>';
            else
                echo '<option value="'.$i.'">'.$i.'</option>';
        }
        ?>
      </select>
      <select name="MES_NACI_HIJO_3" id="MES_NACI_HIJO_3">
      <option></option>
        <?php
	for($m = 1; $m<=12; $m++)
	{
		if($m<10)
			$me = "0" . $m;
		else
			$me = $m;
			
		switch($me)
		{
			case "01": $mes = "Enero"; break;
			case "02": $mes = "Febrero"; break;
			case "03": $mes = "Marzo"; break;
			case "04": $mes = "Abril"; break;
			case "05": $mes = "Mayo"; break;
			case "06": $mes = "Junio"; break;
			case "07": $mes = "Julio"; break;
			case "08": $mes = "Agosto"; break;
			case "09": $mes = "Septiembre"; break;
			case "10": $mes = "Octubre"; break;
			case "11": $mes = "Noviembre"; break;
			case "12": $mes = "Diciembre"; break;			
		}
		echo "<option value='$me'>$mes</option>";
	}
		
      ?>
      </select>
      <select name="DIA_NACI_HIJO_3" id="DIA_NACI_HIJO_3">
      <option></option>
        <?php
	for($d=1;$d<=31;$d++)  
	{
		if($d<10) 
			$dd = "0" . $d; 
		else
			$dd = $d; 
		echo "<option value='$dd'>$dd</option>";
	}	
		
      ?>
    </select></td>
    <td><select name="N_ACADEMICO_HIJO3" class="Tabla" id="N_ACADEMICO_HIJO3">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option></option>  
      <option>PRIMARIA</option>
      <option>BACHILLERATO</option>
      <option>TECNICO</option>
      <option>TECNOLOGICO</option>
      <option>PROFECIONAL</option>
      <option>POSGRADO</option>
    </select></td>
    <td colspan="6"><select name="OCUPACION_HIJO3" class="Tabla" id="OCUPACION_HIJO3">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option></option>  
      <option>ESTUDIANTE</option>
      <option>EMPLEADO</option>
      <option>INDEPENDIENTE</option>
      <option>DESEMPLEADO</option>
    </select></td>
  </tr>
  <tr>
    <td class="preguntas">4</td>
    <td><input name="NOMBRE_HIJO_4" type="text" class="campos_formulario" id="NOMBRE_HIJO_4" size="20" /></td>
    <td><select name="GENERO_HIJO_4" class="Tabla" id="GENERO_HIJO_4">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option></option>  
      <option>MASC</option>
      <option>FEM</option>
    </select></td>
    <td>
    <!-- <input name="FECHA_INI6" type="text" class="tcal" id="FECHA_INI6" value="" size="10" readonly="readonly" /> -->
    
      <select name="ANO_NACI_HIJO_4" id="ANO_NACI_HIJO_4">
      <option></option>
        <?php
        for($i=date('o'); $i>=1910; $i--){
            if ($i == date('o'))
                echo '<option value="'.$i.'">'.$i.'</option>';
            else
                echo '<option value="'.$i.'">'.$i.'</option>';
        }
        ?>
      </select>
      <select name="MES_NACI_HIJO_4" id="MES_NACI_HIJO_4">
      <option></option>
        <?php
	for($m = 1; $m<=12; $m++)
	{
		if($m<10)
			$me = "0" . $m;
		else
			$me = $m;
			
		switch($me)
		{
			case "01": $mes = "Enero"; break;
			case "02": $mes = "Febrero"; break;
			case "03": $mes = "Marzo"; break;
			case "04": $mes = "Abril"; break;
			case "05": $mes = "Mayo"; break;
			case "06": $mes = "Junio"; break;
			case "07": $mes = "Julio"; break;
			case "08": $mes = "Agosto"; break;
			case "09": $mes = "Septiembre"; break;
			case "10": $mes = "Octubre"; break;
			case "11": $mes = "Noviembre"; break;
			case "12": $mes = "Diciembre"; break;			
		}
		echo "<option value='$me'>$mes</option>";
	}
		
      ?>
      </select>
      <select name="DIA_NACI_HIJO_4" id="DIA_NACI_HIJO_4">
      <option></option>
        <?php
	for($d=1;$d<=31;$d++)  
	{
		if($d<10) 
			$dd = "0" . $d; 
		else
			$dd = $d; 
		echo "<option value='$dd'>$dd</option>";
	}	
		
      ?>
    </select></td>
    <td><select name="N_ACADEMICO_HIJO4" class="Tabla" id="N_ACADEMICO_HIJO4">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option></option>  
      <option>PRIMARIA</option>
      <option>BACHILLERATO</option>
      <option>TECNICO</option>
      <option>TECNOLOGICO</option>
      <option>PROFECIONAL</option>
      <option>POSGRADO</option>
    </select></td>
    <td colspan="6"><select name="OCUPACION_HIJO4" class="Tabla" id="OCUPACION_HIJO4">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option></option>  
      <option>ESTUDIANTE</option>
      <option>EMPLEADO</option>
      <option>INDEPENDIENTE</option>
      <option>DESEMPLEADO</option>
    </select></td>
  </tr>
  <tr>
    <td class="preguntas">5</td>
    <td><input name="NOMBRE_HIJO_5" type="text" class="campos_formulario" id="NOMBRE_HIJO_5" size="20" /></td>
    <td><select name="GENERO_HIJO_5" class="Tabla" id="GENERO_HIJO_5">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option></option>  
      <option>MASC</option>
      <option>FEM</option>
    </select></td>
    <td>
    <!-- <input name="FECHA_INI7" type="text" class="tcal" id="FECHA_INI7" value="" size="10" readonly="readonly" /> -->
    
      <select name="ANO_NACI_HIJO_5" id="ANO_NACI_HIJO_5">
      <option></option>
        <?php
        for($i=date('o'); $i>=1910; $i--){
            if ($i == date('o'))
                echo '<option value="'.$i.'">'.$i.'</option>';
            else
                echo '<option value="'.$i.'">'.$i.'</option>';
        }
        ?>
      </select>
      <select name="MES_NACI_HIJO_5" id="MES_NACI_HIJO_5">
      <option></option>
        <?php
	for($m = 1; $m<=12; $m++)
	{
		if($m<10)
			$me = "0" . $m;
		else
			$me = $m;
			
		switch($me)
		{
			case "01": $mes = "Enero"; break;
			case "02": $mes = "Febrero"; break;
			case "03": $mes = "Marzo"; break;
			case "04": $mes = "Abril"; break;
			case "05": $mes = "Mayo"; break;
			case "06": $mes = "Junio"; break;
			case "07": $mes = "Julio"; break;
			case "08": $mes = "Agosto"; break;
			case "09": $mes = "Septiembre"; break;
			case "10": $mes = "Octubre"; break;
			case "11": $mes = "Noviembre"; break;
			case "12": $mes = "Diciembre"; break;			
		}
		echo "<option value='$me'>$mes</option>";
	}
		
      ?>
      </select>
      <select name="DIA_NACI_HIJO_5" id="DIA_NACI_HIJO_5">
      <option></option>
        <?php
	for($d=1;$d<=31;$d++)  
	{
		if($d<10) 
			$dd = "0" . $d; 
		else
			$dd = $d; 
		echo "<option value='$dd'>$dd</option>";
	}	
		
      ?>
    </select></td>
    <td><select name="N_ACADEMICO_HIJO5" class="Tabla" id="N_ACADEMICO_HIJO5">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option></option>  
      <option>PRIMARIA</option>
      <option>BACHILLERATO</option>
      <option>TECNICO</option>
      <option>TECNOLOGICO</option>
      <option>PROFECIONAL</option>
      <option>POSGRADO</option>
    </select></td>
    <td colspan="6"><select name="OCUPACION_HIJO5" class="Tabla" id="OCUPACION_HIJO5">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option></option>  
      <option>ESTUDIANTE</option>
      <option>EMPLEADO</option>
      <option>INDEPENDIENTE</option>
      <option>DESEMPLEADO</option>
    </select></td>
  </tr>
  <tr>
    <td height="27" class="preguntas">6</td>
    <td><input name="NOMBRE_HIJO_6" type="text" class="campos_formulario" id="NOMBRE_HIJO_6" size="20" /></td>
    <td><select name="GENERO_HIJO_6" class="Tabla" id="GENERO_HIJO_6">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option></option>  
      <option>MASC</option>
      <option>FEM</option>
    </select></td>
    <td>
    <!--<input name="FECHA_INI8" type="text" class="tcal" id="FECHA_INI8" value="" size="10" readonly="readonly" />  -->
      
      
      <select name="ANO_NACI_HIJO_6" id="ANO_NACI_HIJO_6">
      <option></option>
        <?php
        for($i=date('o'); $i>=1910; $i--){
            if ($i == date('o'))
                echo '<option value="'.$i.'">'.$i.'</option>';
            else
                echo '<option value="'.$i.'">'.$i.'</option>';
        }
        ?>
      </select>
      <select name="MES_NACI_HIJO_6" id="MES_NACI_HIJO_6">
      <option></option>
        <?php
	for($m = 1; $m<=12; $m++)
	{
		if($m<10)
			$me = "0" . $m;
		else
			$me = $m;
			
		switch($me)
		{
			case "01": $mes = "Enero"; break;
			case "02": $mes = "Febrero"; break;
			case "03": $mes = "Marzo"; break;
			case "04": $mes = "Abril"; break;
			case "05": $mes = "Mayo"; break;
			case "06": $mes = "Junio"; break;
			case "07": $mes = "Julio"; break;
			case "08": $mes = "Agosto"; break;
			case "09": $mes = "Septiembre"; break;
			case "10": $mes = "Octubre"; break;
			case "11": $mes = "Noviembre"; break;
			case "12": $mes = "Diciembre"; break;			
		}
		echo "<option value='$me'>$mes</option>";
	}
		
      ?>
      </select>
      <select name="DIA_NACI_HIJO_6" id="DIA_NACI_HIJO_6">
      <option></option>
        <?php
	for($d=1;$d<=31;$d++)  
	{
		if($d<10) 
			$dd = "0" . $d; 
		else
			$dd = $d; 
		echo "<option value='$dd'>$dd</option>";
	}	
		
      ?>
    </select></td>
    <td><select name="N_ACADEMICO_HIJO6" class="Tabla" id="N_ACADEMICO_HIJO6">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option></option>  
      <option>PRIMARIA</option>
      <option>BACHILLERATO</option>
      <option>TECNICO</option>
      <option>TECNOLOGICO</option>
      <option>PROFECIONAL</option>
      <option>POSGRADO</option>
    </select></td>
    <td colspan="6"><select name="OCUPACION_HIJO6" class="Tabla" id="OCUPACION_HIJO6">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option></option>  
      <option>ESTUDIANTE</option>
      <option>EMPLEADO</option>
      <option>INDEPENDIENTE</option>
      <option>DESEMPLEADO</option>
    </select></td>
  </tr>
  
  <tr bgcolor="#CCCCCC" class="titulos">
    <td colspan="11" bgcolor="#D00">VI. TALLAS DE DOTACIÓN</td>
  </tr>
  <tr>
    <td class="preguntas">CAMISA</td>
    <td>
    <select name="CAMISA" class="Tabla" id="CAMISA">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>XS</option>
      <option>S</option>
      <option>SI</option>
      <option>M</option>
      <option>L</option>
      <option>XL</option>
      <option>XXL</option>
    </select></td>
    <td class="preguntas">CHAQUETA</td>
    <td><select name="CHAQUETA" class="Tabla" id="CHAQUETA">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>XS</option>
      <option>S</option>
      <option>SI</option>
      <option>M</option>
      <option>L</option>
      <option>XL</option>
      <option>XXL</option>
    </select></td>
    <td class="preguntas">PANTALON</td>
    <td colspan="6"><select name="PANTALON" class="Tabla" id="PANTALON">
      <?PHP       
		//  echo "<option>".$TIPO_DOCUMENTO."</option>";		      
		?>
      <option>28</option>
      <option>30</option>
      <option>32</option>
      <option>34</option>
      <option>36</option>
      <option>38</option>
      <option>40</option>
      <option>42</option>
      <option>44</option>
    </select></td>
  </tr>
  <tr>
    <td class="preguntas">ZAPATOS</td>
    <td><input name="ZAPATOS" type="text" class="campos_formulario" id="ZAPATOS" size="10" /></td>
    <td class="preguntas">GUANTES</td>
    <td><input name="GUANTES" type="text" class="campos_formulario" id="GUANTES" size="10" /></td>
    <td class="preguntas">&nbsp;</td>
    <td colspan="6">&nbsp;</td>
  </tr>
  
  <tr>
    <td height="27" colspan="11" class="preguntas" align="center">
    <input name="enviaformventa" type="button" id="enviaformventa2" onclick="pregunta()" value="GUARDAR FORMULARIO" /> 
    </td>
    </tr>
</table>
</form>
</body>
</html>