<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Registrese a Page of Inventarios COMCAP</title>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.name; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' es requerido.\n'; }
  } if (errors) alert('Han ocurido los siguientes errores:\n'+errors);
  document.MM_returnValue = (errors == '');
}
//-->
</script>
<style type="text/css">
<!--
.Estilo2 {font-family: Verdana, Arial, Helvetica, sans-serif}
.Estilo3 {
	font-size: 12px;
	font-weight: bold;
	font-style: italic;
	color: #000000;
}
-->
</style>
<link href="../estilos.css" rel="stylesheet" type="text/css" />
<style type="text/css">
.Estilo11 {color: #FF0000}
.Estilo6 {	color: #FFFFFF;
	text-align: center;
}
</style>
</head>

<body>

  <!--Estas lineas de codigo crean una tabla-->
<table width="61%"  border="0" align="center">
  <tr>
    <th align="center" valign="middle" class="Estilo2"><img src="../../imagenes/logoAdministracion.png" width="213" height="73"></th>
  </tr>
<form action="registrar.php" method="post" name="ingreso" class="campos_formulario" onSubmit="MM_validateForm('Nombre','','R','Apellido','','R','Nick','','R','Password','','R');return document.MM_returnValue">
<tr align="center">
<td width="100%" height="206" align="center" valign="middle"><div align="center" class="Estilo2 Estilo3">
  <p>&nbsp;</p>
  <table width="373" height="157"  border="0" align="center">
    <tr>
      <td width="367" align="center" valign="top"><div align="center">
        <h3><img src="../../imagenes/reg-usuario.png" alt="" width="367" height="45" /></h3>
      </div></td>
    </tr>
    <tr background="../../imagenes/imagenesintranetpeoplemarketing/web-plataforma-PM-09.jpg">
      <td valign="top"><div >
        <h3><span class="Estilo7 Estilo6">&nbsp;&nbsp;Nombres :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
          <input name="nombre" type="text" class="campos_formulario" id="nombre" value="Nombres Usuario" size="20" onclick="value=''" />
        </h3>
      </div>
        <div>
          <h3><span class="Estilo7 Estilo6">&nbsp;&nbsp;Apellidos :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <input name="apellido" type="text" class="campos_formulario" id="apellido" value="Apellidos Usuario" size="20" onclick="value=''" />
          </h3>
        </div>
        <div>
          <h3><span class="Estilo7 Estilo6">&nbsp;&nbsp;Tipo Documento :&nbsp;</span>
            <select name="tipocc" class="campos_formulario" id="tipocc" onchange="document.InsertarCliente.Identificacion.disabled=false">
              <option>---Tipo Documento---</option>
              <option value="CC">C.C.</option>
              <option value="TI">T.I.</option>
              <option value="NIT">NIT</option>
              <option value="CEx">Ced&ugrave;la de Extranjeria</option>
            </select>
          </h3>
        </div>
        <div>
          <h3><span class="Estilo7 Estilo6">&nbsp;&nbsp;Identificaci&oacute;n :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <input name="identificacion" type="text" class="campos_formulario" id="identificacion" onclick="value=''" value="Numero de Documento" size="20"/>
          </h3>
        </div>
        <div>
          <h3><a href="users/inicio.html" target="_self" class="Estilo11"></a><span class="Estilo7 Estilo6">&nbsp;&nbsp;Cargo :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <input name="cargo" type="text" class="campos_formulario" id="Usuario" value="Digite el Cargo" size="20" onclick="value=''" />
          </h3>
        </div>
        <div>
          <h3><a href="users/inicio.html" target="_self" class="Estilo11"></a><span class="Estilo7 Estilo6">&nbsp;&nbsp;E-Mail :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <input name="email" type="text" class="campos_formulario" id="email" value="ejem: correo@hotmail.com" size="20" onclick="value=''" />
          </h3>
        </div>
        <div>
          <h3><a href="users/inicio.html" target="_self" class="Estilo11"></a><span class="Estilo7 Estilo6">&nbsp;&nbsp;Telefono :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <input name="telefono" type="text" class="campos_formulario" id="telefono" value="Digite aqui un numero de tel" size="20" onclick="value=''" />
          </h3>
        </div>
        <div>
          <h3><a href="users/inicio.html" target="_self" class="Estilo11"></a><span class="Estilo7 Estilo6">&nbsp;&nbsp;Direcci&oacute;n :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <input name="direccion" type="text" class="campos_formulario" id="direccion" value="Digite aqui la direccion" size="20" onclick="value=''" />
          </h3>
        </div>
        <div>
          <h3><a href="users/inicio.html" target="_self" class="Estilo11"></a><span class="Estilo7 Estilo6">&nbsp;&nbsp;Usuario :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <input name="nick" type="text" class="campos_formulario" id="nick" onclick="value=''" value="Digite su Usuario" size="20" />
          </h3>
        </div>
        <div>
          <h3><a href="users/inicio.html" target="_self" class="Estilo11"></a><span class="Estilo7 Estilo6">&nbsp;&nbsp;Contrase&ntilde;a :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <input name="password" type="password" class="campos_formulario" id="Pass" value="Digite" size="20" onclick="value=''" />
          </h3>
        </div></td>
    </tr>
    <tr background="../../imagenes/lin-form.png">
      <td><div align="center">
        <p class="Estilo6">
          <input name="AgregarU" type="submit" class="botones" id="AgregarU" value="Agregar" alt="Agregar usuario" />
          &nbsp;
          <input name="reset" type="reset" class="botones" id="reset" value="Limpiar" />
        </p>
      </div></td>
    </tr>
  </table>
  <br>
  <br />
<a href=javascript:history.back()>REGRESAR>>>></a><br><br>
</div></td></tr>
</form>
</table>
</body>
</html>