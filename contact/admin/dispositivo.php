<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Ingreso de dispositivos :::::::::::::COMCAP</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../estilos.css" rel="stylesheet" type="text/css">
<style type="text/css">@import url(calendar-green.css);</style>
<script type="text/javascript" src="../jscalendar-1.0/calendar.js"></script>
<script type="text/javascript" src="../jscalendar-1.0/lang/calendar-en.js"></script>
<script type="text/javascript" src="../jscalendar-1.0/calendar-setup.js"></script>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.name; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' e-mail es una dirección de correo eléctronico.\n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' debe digitar numeros.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' es requerido.\n'; }
  } if (errors) alert('Ha ocurrido los siguientes errores:\n'+errors);
  document.MM_returnValue = (errors == '');
}
//-->
</script>
</head>
<body>
<?PHP
 require('include_sup.php'); //trae la pagina//
?>
<div align="center">
   <!-- Estas lineas de codigo crean una tabla -->
<table width="742"  border="0">
  <tr>
    <th height="23" class="titulo" scope="col">INGRESO DE DISPOSITIVO <br>   </th>
    </tr>
  <tr>
    <td><br>
      <br>

      <form action="Ingreso_dispositivo.php" method="post" name="InsertarCliente" id="InsertarCliente" onSubmit="MM_validateForm('REFERENCIA','','R','NOMBRE_DEL_DISPOSITIVO','','R','UNIDADES','','RisNum','DESCRIPCION_DEL_DISPOSITIVO','','R');return document.MM_returnValue">
    <table width="1%"  border="1" align="center" cellpadding="5" cellspacing="5" class="titulo">
      <tr>
        <th width="32%" class="texto_formulario" scope="col"><div align="left">
          <LABEL for="checkbox_row_2">referencia</LABEL>
        </div></th>
        <th width="68%" scope="col"><div align="left">
          <input name="REFERENCIA" type="text" class="campos_formulario" id="REFERENCIA" value="Marca y referencia"  onClick="value=''"size="30">
        </div></th>
        </tr>
      <tr>
        <th class="texto_formulario"><div align="left">nOMBRE DEL DISPOSITIVO:</div></th>
        <td><div align="left">
          <input name="NOMBRE_DEL_DISPOSITIVO" type="text" class="campos_formulario" id="NOMBRE_DEL_DISPOSITIVO" onClick="value=''" value="Nombre de dispositivo" size="30">
        </div></td>
        </tr>
      <tr>
        <th class="texto_formulario"><div align="left">dESCRIPCI&oacute;N DEL DISPOSITIVO:</div></th>
        <td align="left" valign="middle">
          <div align="left">
            <textarea name="DESCRIPCION_DEL_DISPOSITIVO" cols="30" rows="5" class="campos_formulario" id="DESCRIPCION_DEL_DISPOSITIVO"  onClick="value=''">Breve descripción</textarea>
          </div></td>
      </tr>
      <!-- <tr>
        <th class="texto_formulario"><div align="left">fECHA DE INGRESO:</div></th>
        <td><div align="left">
          <input name="FECHA_DE_INGRESO" type="text" class="campos_formulario" id="FECHA_DE_INGRESO"value="a&ntilde;o-mes-dia" onClick="value=''"  size="30">
        <input name="data" type="text" id="data" value="Clic aca" size="5" />

		</div></td>
        </tr> -->
      <tr>
        <th height="32" class="texto_formulario"><div align="left">UNIDADES:</div></th>
        <td><div align="left">
          <input name="UNIDADES" type="text" class="campos_formulario" id="UNIDADES"value="Cantidad de dispositivos"  onClick="value=''" size="30">
        </div></td>
        </tr>
      <tr>
        <TD height="35" colspan="2" align="center" valign="middle" class="texto_formulario">
            <div align="center">
            <input name="Agregar" type="submit" class="botones" id="Agregar" value="Agregar">
        &nbsp;
              <input name="reset" type="reset" class="botones" id="reset" value="Limpiar">
            </div></TD>
        </tr>
      </table>
      </form>
	<!--   <script type="text/javascript">
Calendar.setup(
{
inputField : "data", // ID of the input field
ifFormat : "%Y %m, %d", // the date format
button : "trigger" // ID of the button
}
);
</script> -->
	  </td>
    </tr>
  <tr> 
    <td><br>
      <br>      <?php include('include_inf.php'); ?></td> <!-- trae la pagina y la incluye en el boton -->
    </tr>
</table>
</div>
</body>
</html>
