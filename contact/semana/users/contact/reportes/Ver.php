<?php
if(!defined('verificacion_entrada') || !verificacion_entrada) die('No es un punto de acceso vlido');

require_once('modulos/Reportes/Reporte.php');
global $ruta_smarty, $modulo;
$ruta_smarty_modulo = $ruta_smarty."/modulos/".$modulo;
if(isset($_REQUEST['registro']) && $_REQUEST['registro']>0){
	$reporte = new Reporte();	
	$reporteSeleccionado = $reporte->recuperar($_REQUEST['registro']);
	if(count($reporteSeleccionado)>0){		
		if($reporteSeleccionado['tipo']=='G'){
			$rutaReporte = 'modulos/Reportes/ReportesGenericos/'.$reporteSeleccionado['nombre'];
		}elseif($reporteSeleccionado['tipo']=='E'){
			$rutaReporte = 'modulos/Reportes/ReportesCliente/Cliente_'
						  .$reporteSeleccionado['cliente_id'].'/'.$reporteSeleccionado['nombre'];
		}		
		$rutaVerReporte = $rutaReporte.'/Ver.php';
		if(file_exists($rutaVerReporte)){
			require_once($rutaVerReporte);
		}else{
			__M($modulo_strings['ERR_NO_ARCHIVO_REPORTE']);
		}
	}else{
		__M($modulo_strings['ERR_NO_TIENE_PERMISOS']);
	}
}else{
	__M($modulo_strings['ERR_NO_REPORTE_ID']);
}
?>