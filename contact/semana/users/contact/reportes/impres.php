<style type="text/css">
/* --------------- BOTONES --------------- */

.button, .button:visited { /* botones genéricos */
	background: #222 url(http://sites.google.com/site/zavaletaster/Home/overlay.png) repeat-x;
	display: inline-block;
	padding: 5px 10px 6px;
	color: #000099;
	text-decoration: none;
	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;
	-moz-box-shadow: 0 1px 3px rgba(0,0,0,0.6);
	-webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.6);
	text-shadow: 0 -1px 1px rgba(0,0,0,0.25);
	border-top: 0px;
	border-left: 0px;
	border-right: 0px;
	border-bottom: 1px solid rgba(0,0,0,0.25);
	position: relative;
	cursor:pointer;
}

button::-moz-focus-inner,
input[type="reset"]::-moz-focus-inner,
input[type="button"]::-moz-focus-inner,
input[type="submit"]::-moz-focus-inner,
input[type="file"] > input[type="button"]::-moz-focus-inner {
border: none;
}

.button:hover { /* el efecto hover */
background-color: #111
color: #FFF;
}

.button:active{ /* el efecto click */
top: 1px;
}

/* botones pequeños */
.small.button, .small.button:visited {
font-size: 11px ;
}

/* botones medianos */
.button, .button:visited,.medium.button, .medium.button:visited {
font-size: 13px;
font-weight: bold;
line-height: 1;
text-shadow: 0 -1px 1px rgba(0,0,0,0.25);
}

/* botones grandes */
.large.button, .large.button:visited {
font-size:14px;
padding: 8px 14px 9px;
}

/* botones extra grandes */
.super.button, .super.button:visited {
font-size: 34px;
padding: 8px 14px 9px;
}

.pink.button {
	background-color: #009966;
}
.pink.button:hover{ background-color: #C81E82; }

.green.button, .green.button:visited { background-color: #91BD09; }
.green.button:hover{ background-color: #749A02; }

.red.button, .red.button:visited { background-color: #E62727; }
.red.button:hover{ background-color: #CF2525; }

.orange.button, .orange.button:visited { background-color: #FF5C00; }
.orange.button:hover{ background-color: #D45500; }

.blue.button, .blue.button:visited { background-color: #2981E4; }
.blue.button:hover{ background-color: #2575CF; }

.yellow.button, .yellow.button:visited { background-color: #FFB515; }
.yellow.button:hover{ background-color: #FC9200; }
</style>


<?php  
//incluimos la libreria html2fpdf  
include_once ('html2fpdf.php');  
// Guardamos en una variable el texto que contendra el pdf  
$pdf = "  
<html>  
<head>  
<title>Titulo del archivo pdf</title>  
</head>  
<body>  
<p>Este es el texto del archivo pdf. Podemos incluir imagenes, enlaces, etc.</p>  
</body>  
</html>  
";  
$pdf = new html2fpdf(); // Generamos un objeto nuevo html2fpdf  
$pdf -> AddPage(); // Añadimos una página  
$pdf -> WriteHTML($pdf); // Indicamos la variable con el contenido que queremos incluir en el pdf  
$pdf -> Output('archivo_pdf.pdf', 'D'); //Generamos el archivo "archivo_pdf.pdf". Ponemos como parametro 'D' para forzar la descarga del archivo.  
?>


<?php
/*
 // La siguiente línea ejecutará una orden en DOS. Esto solo debe ejecutarse una vez.
 // Las comillas hacen que lo ejecute Windows directamente
 `mode com1: BAUD=9600 PARITY=N data=8 stop=1 xon=off`;
 
 //Abrimos el puerto com1
 $fp = fopen ("COM1:", "w+");
 if (!$fp) {
 echo "Error al abrir COM1.";
 } else {
// $datos= escapeshellcmd($_REQUEST["datos"]);
 $datos= "datos";
 fputs ($fp, $datos );
 fclose ($fp);
 echo $datos;
 }*/
?>


<?php
//Es importante filtrar lo que recibimos para que no nos ejecuten comandos
/*$datos= escapeshellcmd($_REQUEST["datos"]);
 
//Enviamos por puerto serie los datos
exec ("echo ".$datos." > /dev/ttyUSB0");
*/
?>



<script>
///////////////////////////////////////////////funcio calculo con valores de una base de datos/////////////////////////////////
function subtotal(price,cant)
{
//Dimensiono name como nuevo arreglo
var name = new Array();
var Total;
//En name guardo un split o recorte del nombre recibido en cant separado por el _
name = cant.split("_");
//Multiplico precio por cantidad
var subtot = price * document.getElementById(cant).value;
//Creo variable con el nombre del subtotal y name[1] que contiene el $row[Nombre]
var subname = 'SubTotal_'+name[1];
//Asigno el valor subtot al cuadro de texto Subtotal del producto correspondiente
document.getElementById(subname).value=subtot;
Total = parseInt(document.getElementById('Total').value) + subtot;
document.getElementById('Total').value = Total;
}
</script>

<form name="form">
<table border=1>
<tr>
<td>Id</td>
<td>Nombre</td>
<td>Valor</td>
<td>Cantidad</td>
<td>Subtotal</td>

<?php
$conn = mysqli_connect("192.168.0.7","Foros","Foros");
mysqli_select_db($conn,"Foros");
$sql = "select * from products";
$query = mysqli_query($conn,$sql);
while ($rows = mysqli_fetch_array($query))
{
echo "<tr><td>".$rows['Id']."</td><td>".$rows['Nombre']."</td><td>".$rows['PriceProd']."</td>";
echo "<td><input type='text' name='Cantidad_$rows[Nombre]' id='Cantidad_$rows[Nombre]' maxlength='5' onblur=subtotal('$rows[PriceProd]','Cantidad_$rows[Nombre]')></td>";
echo "<td><input type='text' name='SubTotal_$rows[Nombre]' id='SubTotal_$rows[Nombre]'></td></tr>";
}
?>

<tr><td colspan="5" align="center">Total: &nbsp;&nbsp;<input type="text" name="Total" id="Total" value="0"><td></tr>
</table>
</form>



<script>
////////////////////////////////muestra el resultado de una suma en ventana emergente/////////////////////////////////////////

function sumar(uno,dos)
{
var total;
total = parseInt(uno.value)+parseInt(dos.value);
alert("El valor es " + total);

}
</script>


<form name="prueba">
uno<input type="text" name="uno">
dos<input type="text" name="dos">
<input type="button" name="Ver total" value="Ver total" onclick="sumar(uno,dos)">
</form>





<script language="javascript">
/////////////////////////////////////////suma valores dependiendo el caso segun una lista desplegable/////////////////////////////

function addTotals() {
with (document.forms["f1"])
{
var totalResult = Number( one.value ) + Number( two.value )+ Number( three.value )+ Number( four.value );
total.value = roundTo( totalResult, 2 );
}

}


function roundTo(num,pow){
if( isNaN( num ) )
{
num = 0;
}

num *= Math.pow(10,pow);
num = (Math.round(num)/Math.pow(10,pow))+ "" ;
if(num.indexOf(".") == -1)
num += "." ;
while(num.length - num.indexOf(".") - 1 < pow)
num += "0" ;

return "$"+num;

}

</script>

<form name="f1">
<p>
<select size="1" style="width:140px;font-size:10px;font-family:Verdana, Arial, Helvetica, sans-serif;font-weight:bold;color:#000000;background-color:#FFFFFF" name='one'>
<option value='0'> # Adultos
<option value='2183'> 1
<option value='4366'> 2
</select>
<br>
<select size="1" style="width:140px;font-size:10px;font-family:Verdana, Arial, Helvetica, sans-serif;font-weight:bold;color:#000000;background-color:#FFFFFF" name='two'>
<option value='0'> # 4 a 11 años
<option value='1308'> 1
<option value='2616'> 2
</select>
<br>
<select size="1" style="width:140px;font-size:10px;font-family:Verdana, Arial, Helvetica, sans-serif;font-weight:bold;color:#000000;background-color:#FFFFFF" name='three'>
<option value='0'> # 2 a 3 años
<option value='551'> 1
<option value='1102'> 2
</select>
<br>
<select size="1" style="width:140px;font-size:10px;font-family:Verdana, Arial, Helvetica, sans-serif;font-weight:bold;color:#000000;background-color:#FFFFFF" name='four'>
<option value='0'> # Single
<option value='4346'> 1
<option value='8692'> 2
</select>
</p>
<p>
<input type="button" name="button" onclick="addTotals()" value="Calcular" style="width:55px;font-size:10px;font-family:Verdana, Arial, Helvetica, sans-serif;font-weight:bold;color:#000000">
<input type="text" name="total" style="width:76px;font-size:10px;font-family:Verdana, Arial, Helvetica, sans-serif;font-weight:bold;color:#000000;background-color:#FFFFFF">
</p>
</form>






<!-- /////////////////////////////////////////////// suma valores automaticos ///////////////////////////////////////////// -->

<form name="miForm" action="">
    <input type="text" name="ValRepuesto" onblur="suma()" /><br/>
    <input type="text" name="ValRepuesto" onblur="suma()" /><br/>
    <input type="text" name="ValRepuesto" onblur="suma()" /><br/>
    <input type="text" name="ValRepuesto" onblur="suma()" /><br/>
    <input type="text" name="ValRepuesto" onblur="suma()" /><br/>
    <input type="text" name="ValRepuesto" onblur="suma()" /><br/>
    <input type="text" name="ValRepuesto" onblur="suma()" /><br/>
    <hr/>
    <input type="text" name="TotalValor" />
</form>


<script type="text/javascript">
<!--

function suma(){
    var camposValRepuesto = document.miForm["ValRepuesto"];
    var camposValRepuesto_num = camposValRepuesto.length;
    var total = 0;
    for(var i=0, total=0, valor; i<camposValRepuesto_num; i++) {
        valor = parseFloat( camposValRepuesto[i].value );
        if( !isNaN(valor) )
            total += valor;
    }
    document.miForm["TotalValor"].value = total;
}

// -->
</script>  