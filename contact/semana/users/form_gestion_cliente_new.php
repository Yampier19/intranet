<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
<script src="Scripts/swfobject_modified.js" type="text/javascript"></script>
<script type="text/javascript">
function MM_validateForm() { //v4.0
  if (document.getElementById){
    var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
    for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=document.getElementById(args[i]);
      if (val) { nm=val.name; if ((val=val.value)!="") {
        if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
          if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
        } else if (test!='R') { num = parseFloat(val);
          if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
          if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
            min=test.substring(8,p); max=test.substring(p+1);
            if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
      } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
    } if (errors) alert('The following error(s) occurred:\n'+errors);
    document.MM_returnValue = (errors == '');
} }
</script>

<script>
function califiOnChange(sel) {
      if (sel.value=="CONTACTOS_EFECTIVOS"){
           divC = document.getElementById("pCall");
           divC.style.display = "";
		   
		   divC = document.getElementById("pCall2");
           divC.style.display = "";
		   
		   divC = document.getElementById("pNventa3");
           divC.style.display = "none";

           divT = document.getElementById("pMedia");
           divT.style.display = "none";
		   
		   divC = document.getElementById("pBtl");
           divC.style.display = "none";

           divT = document.getElementById("pAdmin");
           divT.style.display = "none";
         }
		   
	  if (sel.value=="CONTACTOS_NO_EFECTIVOS"){

           divC = document.getElementById("pCall");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pCall2");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa3");
           divC.style.display = "none";

           divT = document.getElementById("pMedia");
           divT.style.display = "";
		   
		   divC = document.getElementById("pBtl");
           divC.style.display = "none";

           divT = document.getElementById("pAdmin");
           divT.style.display = "none";
		   
		   divC = document.getElementById("pNventa");
           divC.style.display = "none";
		   
           divC = document.getElementById("pNventa3");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa4");
           divC.style.display = "none";
       }
	   
	  if (sel.value=="NO_CONTACTOS"){

           divC = document.getElementById("pCall");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pCall2");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa3");
           divC.style.display = "none";

           divT = document.getElementById("pMedia");
           divT.style.display = "none";
		   
		   divC = document.getElementById("pBtl");
           divC.style.display = "";

           divT = document.getElementById("pAdmin");
           divT.style.display = "none";
		   
		   divC = document.getElementById("pNventa");
           divC.style.display = "none";
		   
           divC = document.getElementById("pNventa3");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa4");
           divC.style.display = "none";
       }
	   
	  if (sel.value=="INCONSISTENCIAS"){

           divC = document.getElementById("pCall");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pCall2");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa3");
           divC.style.display = "none";

           divT = document.getElementById("pMedia");
           divT.style.display = "none";
		   
		   divC = document.getElementById("pBtl");
           divC.style.display = "none";

           divT = document.getElementById("pAdmin");
           divT.style.display = "";
		   
		   divC = document.getElementById("pNventa");
           divC.style.display = "none";
		   
           divC = document.getElementById("pNventa3");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa4");
           divC.style.display = "none";
       }  
}

function calOnChange(sel) {
      if (sel.value=="NO_VENTA"){
           divC = document.getElementById("pCall2");
           divC.style.display = "";
		   
		   divC = document.getElementById("pCall3");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa");
           divC.style.display = "";
		   
         }
		   
	  if (sel.value=="VENTA"){
           divC = document.getElementById("pCall2");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pCall3");
           divC.style.display = "";
		   
		   divC = document.getElementById("pNventa");
           divC.style.display = "none";
		   
           divC = document.getElementById("pNventa3");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa4");
           divC.style.display = "none";

       }	   	 
}

function nventaOnChange(sel) {
      if (sel.value=="PERMANENCIA_ACTIVA_CON_OTRO_OPERADOR"){
           divC = document.getElementById("pNventa");
           divC.style.display = "";
		   
		   divC = document.getElementById("pNventa2");
           divC.style.display = "none";
		   
           divC = document.getElementById("pNventa3");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa4");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa5");
           divC.style.display = "none";
		   
           divC = document.getElementById("pNventa6");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa7");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa8");
           divC.style.display = "none";
		   
         }
		 
	  if (sel.value=="YA_TIENE_EL_SERVICIO_OFRECIDO"){
           divC = document.getElementById("pNventa");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa2");
           divC.style.display = "";
		   
           divC = document.getElementById("pNventa3");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa4");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa5");
           divC.style.display = "none";
		   
           divC = document.getElementById("pNventa6");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa7");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa8");
           divC.style.display = "none";
		   
         }	 
		   	  
	  if (sel.value=="NO_LE_PARECE_ATRACTIVA_LA_OFERTA"){
		   divC = document.getElementById("pNventa");
           divC.style.display = "none"; 
		   
		   divC = document.getElementById("pNventa2");
           divC.style.display = "none";
		  
           divC = document.getElementById("pNventa3");
           divC.style.display = "";
		   
		   divC = document.getElementById("pNventa4");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa5");
           divC.style.display = "none";
		   
           divC = document.getElementById("pNventa6");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa7");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa8");
           divC.style.display = "none";

       }
	   
	  if (sel.value=="PROCESOS_DE_SERVICIO"){
		   divC = document.getElementById("pNventa");
           divC.style.display = "none"; 
		   
		   divC = document.getElementById("pNventa2");
           divC.style.display = "none";
		  
           divC = document.getElementById("pNventa3");
           divC.style.display = "none";
		   
		   divT = document.getElementById("pNventa4");
           divT.style.display = "";
		   
		   divC = document.getElementById("pNventa5");
           divC.style.display = "none";
		   
           divC = document.getElementById("pNventa6");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa7");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa8");
           divC.style.display = "none";

       }
	   
	  if (sel.value=="PENDIENTE_CIERRE_DE_VENTA"){
           divC = document.getElementById("pNventa");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa2");
           divC.style.display = "none";
		   
           divC = document.getElementById("pNventa3");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa4");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa5");
           divC.style.display = "";
		   
           divC = document.getElementById("pNventa6");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa7");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa8");
           divC.style.display = "none";
		   
         }
		 
	  if (sel.value=="CARTERA_MARCACION_PREDICTIVA"){
           divC = document.getElementById("pNventa");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa2");
           divC.style.display = "";
		   
           divC = document.getElementById("pNventa3");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa4");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa5");
           divC.style.display = "none";
		   
           divC = document.getElementById("pNventa6");
           divC.style.display = "";
		   
		   divC = document.getElementById("pNventa7");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa8");
           divC.style.display = "none";
		   
         }	 
		   	  
	  if (sel.value=="RECHAZADO_EVIDENTE"){
		   divC = document.getElementById("pNventa");
           divC.style.display = "none"; 
		   
		   divC = document.getElementById("pNventa2");
           divC.style.display = "";
		  
           divC = document.getElementById("pNventa3");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa4");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa5");
           divC.style.display = "none";
		   
           divC = document.getElementById("pNventa6");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa7");
           divC.style.display = "";
		   
		   divC = document.getElementById("pNventa8");
           divC.style.display = "none";

       }
	   
	  if (sel.value=="CLIENTE_PYME"){
		   divC = document.getElementById("pNventa");
           divC.style.display = "none"; 
		   
		   divC = document.getElementById("pNventa3");
           divC.style.display = "none";
		  
           divC = document.getElementById("pNventa2");
           divC.style.display = "";
		   
		   divT = document.getElementById("pNventa4");
           divT.style.display = "none";
		   
		   divC = document.getElementById("pNventa5");
           divC.style.display = "none";
		   
           divC = document.getElementById("pNventa6");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa7");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa8");
           divC.style.display = "";

       }
	   
	  if (sel.value=="NO_TIENE _COMPUTADOR"){
		   divC = document.getElementById("pNventa");
           divC.style.display = "none"; 
		   
		   divC = document.getElementById("pNventa3");
           divC.style.display = "none";
		  
           divC = document.getElementById("pNventa2");
           divC.style.display = "";
		   
		   divT = document.getElementById("pNventa4");
           divT.style.display = "none";
		   
		   divC = document.getElementById("pNventa5");
           divC.style.display = "none";
		   
           divC = document.getElementById("pNventa6");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa7");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa8");
           divC.style.display = "";

       }  
	   
	   
	  if (sel.value=="SIN_COBERTURA"){
		   divC = document.getElementById("pNventa");
           divC.style.display = "none"; 
		   
		   divC = document.getElementById("pNventa2");
           divC.style.display = "";
		  
           divC = document.getElementById("pNventa3");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa4");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa5");
           divC.style.display = "none";
		   
           divC = document.getElementById("pNventa6");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa7");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa8");
           divC.style.display = "none";

       }
	   
	  if (sel.value=="NO_APTO_DATACREDITO"){
		   divC = document.getElementById("pNventa");
           divC.style.display = "none"; 
		   
		   divC = document.getElementById("pNventa3");
           divC.style.display = "none";
		  
           divC = document.getElementById("pNventa2");
           divC.style.display = "";
		   
		   divT = document.getElementById("pNventa4");
           divT.style.display = "none";
		   
		   divC = document.getElementById("pNventa5");
           divC.style.display = "none";
		   
           divC = document.getElementById("pNventa6");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa7");
           divC.style.display = "none";
		   
		   divC = document.getElementById("pNventa8");
           divC.style.display = "none";

       }   	   	 
}



function jpg(sel) {
      if (sel.value=="0"){
           divC = document.getElementById("Jpg1pvr");
           divC.style.display = "none";		  
       }
		   
	  if (sel.value=="Voz"){
           divC = document.getElementById("Jpg1pvr");
           divC.style.display = "";		   	
       }	   	 
}


function jpgpvr(sel) {
      if (sel.value=="0"){
           divC = document.getElementById("Jpg1pvr");
           divC.style.display = "none";		  
       }
		   
	  if (sel.value>"0"){
           divC = document.getElementById("Jpg1pvr");
           divC.style.display = "";		   	
       }	   	 
}


function jpgLdistancia(sel) {
      if (sel.value=="0"){
           divC = document.getElementById("Jpg1pvr");
           divC.style.display = "none";		  
       }
		   
	  if (sel.value!="0"){
           divC = document.getElementById("Jpg1pvr");
           divC.style.display = "";		   	
       }	   	 
}


</script>
<style type="text/css">
.subtitulo {
	color: #FFF;
	font-weight: bold;
}
.TITULO {	text-align: center;
	font-weight: bold;
}
.TITULO {	text-align: center;
	color: #FFF;
}
.fuente {	font-size: 9px;
	font-weight: bold;
	text-align: left;
}
.nota {	color: #00F;
}
</style>
</head>

<body>
<?PHP
//session_start();
$string_intro = getenv("QUERY_STRING"); 
parse_str($string_intro);

require('menu_sup_cliente_nuevo.php');
//echo $asesor;
$conexion = mysqli_connect("localhost","wwwpeop_people","Msg4QKKMR52T")or die("no se pudo conectar");
          mysqli_select_db("peoplemarketing",$conexion)or die("no se puede conectar ala base de datos");	  
		  
	//$conex = mysql_connect("localhost","wwwpeop_people","Msg4QKKMR52T")or die("no se pudo conectar");
      //    mysql_select_db("wwwpeop_contact",$conex)or die("no se puede conectar ala base de datos");
	  	 
	$consulasesor = mysqli_query($Conexion,"SELECT * FROM `usuarios` WHERE `Nick` = '".$asesor."' ");	  		  
		  		  
while ($fila2 = mysqli_fetch_array($consulasesor))
    {
		 $CC_ASESOR = ($fila2['Identificacion']);
		 $ASESOR = ($fila2['Nombre']);
		 $Nick = ($fila2['Nick']);
	//}

//echo $asesor;
?>
<!--<form id="form1" name="form1" method="post" action="form_cliente_new.php?asesor=<?php echo $CC_ASESOR; ?>">
<table width="200" border="1" bgcolor="#E0E0E0">
  <tr bgcolor="#CA0000">
    <td width="149">
    <input name="NOMBREASESOR" type="text" class="fuente" id="NOMBREASESOR" value="<?php echo($fila2['Nombre']); ?>" size="15" readonly="readonly"/>&nbsp;</td>
    <td width="633">
<span class="subtitulo"> Cuenta:</span>
<input type="text" name="CUENTA" id="CUENTA" />
        <span class="subtitulo">Numero Telefonico 
        <input type="text" name="TELMARCADO" id="TELMARCADO" />
        </span>
        <input name="button" type="submit" id="button" onclick="MM_validateForm('CUENTA','','RisNum','TELMARCADO','','RisNum');return document.MM_returnValue" value="Enviar" /></td>
    <td width="150">&nbsp;</td>
  </tr>
  <tr bgcolor="#CA0000">
    <td colspan="3">
    -->
<style type="text/css">
.TITULO {
	text-align: center;
	font-weight: bold;
}
.TITULO {
	text-align: center;
	color: #FFF;
}
.fuente {
	font-size: 9px;
	font-weight: bold;
	text-align: left;
}
.subtitulo {
	color: #FFF;
	font-weight: bold;
}
</style>  

<!-- link calendar resources -->
<link rel="stylesheet" type="text/css" href="tcal.css" />
<script type="text/javascript" src="tcal.js"></script>
<table width="900" align="center" bgcolor="#E0E0E0">
  <form id="form1" name="form1" method="post" action="inser_form_ventas.php?asesor=<?php echo $asesor; ?>" enctype="multipart/form-data">
    <tr bgcolor="#CA0000">
      <td colspan="6" align="center" bgcolor="#CA0000" class="TITULO">FORMULARIO CLIENTES REFERIDO</td>
    </tr>
    <tr bgcolor="#CA0000">
      <td colspan="6"><strong class="subtitulo">Datos Personales</strong></td>
    </tr>
    <tr>
      <td width="127" height="38" class="fuente">CUENTA:</td>
      <td width="264" class="fuente"><span class="Estilo3">
        <input name="CUENTA" type="text" class="fuente" id="CUENTA" size="15"/>
      </span></td>
      <td width="125" class="fuente">NUMERO C.C :</td>
      <td colspan="2" class="fuente"><span class="Estilo3">
        <input name="NUMERO_CC" type="text" class="fuente" id="NUMERO_CC" size="15"/>
        *</span></td>
      <td width="140" rowspan="4" class="fuente"><object id="FlashID" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="140" height="140">
        <param name="movie" value="baner calidad/baner_calidad.swf" />
        <param name="quality" value="high" />
        <param name="wmode" value="opaque" />
        <param name="swfversion" value="9.0.45.0" />
        <!-- Esta etiqueta param indica a los usuarios de Flash Player 6.0 r65 o posterior que descarguen la versión más reciente de Flash Player. Elimínela si no desea que los usuarios vean el mensaje. -->
        <param name="expressinstall" value="Scripts/expressInstall.swf" />
        <!-- La siguiente etiqueta object es para navegadores distintos de IE. Ocúltela a IE mediante IECC. -->
        <!--[if !IE]>-->
        <object type="application/x-shockwave-flash" data="baner calidad/baner_calidad.swf" width="140" height="140">
          <!--<![endif]-->
          <param name="quality" value="high" />
          <param name="wmode" value="opaque" />
          <param name="swfversion" value="9.0.45.0" />
          <param name="expressinstall" value="Scripts/expressInstall.swf" />
          <!-- El navegador muestra el siguiente contenido alternativo para usuarios con Flash Player 6.0 o versiones anteriores. -->
          <div>
            <h4>El contenido de esta página requiere una versión más reciente de Adobe Flash Player.</h4>
            <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Obtener Adobe Flash Player" width="112" height="33" /></a></p>
          </div>
          <!--[if !IE]>-->
        </object>
        <!--<![endif]-->
      </object></td>
    </tr>
    <tr>
      <td height="31" class="fuente">NOMBRE:</td>
      <td class="fuente"><span class="Estilo3">
        <input name="NOMBRE" type="text" class="fuente" id="NOMBRE" size="20"/>
        *</span></td>
      <td class="fuente">APELLIDO</td>
      <td colspan="2" class="fuente"><span class="Estilo3">
        <input name="APELLIDO" type="text" class="fuente" id="APELLIDO" size="20"/>
        *</span></td>
    </tr>
    <tr>
      <td height="36" class="fuente">CELULAR:</td>
      <td class="fuente"><span class="Estilo3">
        <input name="CELULAR" type="text" class="fuente" id="CELULAR" size="15"/>
        *</span></td>
      <td class="fuente">TEL CASA</td>
      <td colspan="2" class="fuente"><span class="Estilo3">
        <input name="TELCASA" type="text" class="fuente" id="TELCASA" size="15"/>
        *</span></td>
    </tr>
    <tr>
      <td height="20" class="fuente">TEL OFIC:</td>
      <td class="fuente"><span class="Estilo3">
        <input name="TELOFICI" type="text" class="fuente" id="TELOFICI" size="15"/>
      </span></td>
      <td class="fuente">TEL TELMEX</td>
      <td colspan="2" class="fuente"><span class="Estilo3">
        <input name="TELTELMEX" type="text" class="fuente" id="TELTELMEX" size="15"/>
      </span></td>
    </tr>
    <tr bgcolor="#CA0000">
      <td colspan="6"><strong class="subtitulo">Direccion</strong></td>
    </tr>
    <tr>
      <td class="fuente">DIRECCION :</td>
      <td class="fuente"><span class="Estilo3">
        <input name="DIRECCION" type="text" class="fuente" id="DIRECCION" size="50"/>
        * </span></td>
      <td class="fuente">CIUDAD:</td>
      <td colspan="3" class="fuente"><span class="Estilo3">
        <input name="CIUDAD" type="text" class="fuente" id="CIUDAD" size="15"/>
        *</span></td>
    </tr>
    <tr>
      <td class="fuente">ESTRATO:</td>
      <td class="fuente"><span class="Estilo3">
        <input name="ESTRATO" type="text" class="fuente" id="ESTRATO" size="15"/>
        *</span></td>
      <td class="fuente">&nbsp;</td>
      <td colspan="3" class="fuente">&nbsp;</td>
    </tr>
    <tr bgcolor="#CA0000">
      <td colspan="6"><strong class="subtitulo">Tipificaci&oacute;n</strong></td>
    </tr>
    <tr>
      <td colspan="6"><div align="left"><strong>TIPIFICACI&Oacute;N :</strong>
        <select name="TIPOCONTACTO" id="TIPOCONTACTO" onchange="califiOnChange(this)">
          <option value="CONTACTOS_EFECTIVOS">CONTACTOS EFECTIVOS</option>
          <option value="CONTACTOS_NO_EFECTIVOS">CONTACTOS NO EFECTIVOS</option>
          <option value="NO_CONTACTOS">NO_CONTACTOS</option>
          <option value="INCONSISTENCIAS">INCONSISTENCIAS</option>
        </select>
        
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      Recordar Cliente : 
          <select name="recordarcliente" id="recordarcliente">
            <option>NO</option>
            <option>SI</option>
          </select>
        
      </div></td>
    </tr>
    <tr>
      <td colspan="6"><div id="pCall" style="display:;">
        <div align="left"> Contacto Efectivo :
          <select name="venta" onchange="calOnChange(this)">
            <option value="NO_VENTA">NO VENTA</option>
            <option value="VENTA">VENTA</option>
          </select>
          <br />
        </div>
      </div>
        <div id="pMedia" style="display:none;">
          <div align="left"> Contacto No Efectivo :
            <select name="CONTACTO_NO_EFECTIVO">
              <option>NO_HAY_CONTACTO_CON_EL_TITULAR</option>
              <option>CONTACTO_VIRTUAL</option>
            </select>
            </p>
                        
            
            <input name="tipificar_referido" type="submit" id="tipificar_referido" onclick="MM_validateForm('NOMBRE','','R','APELLIDO','','R','CELULAR','','R','TELCASA','','R');return document.MM_returnValue" value="TIPIFICAR GESTION" />
          </div>
        </div>
        <div id="pBtl" style="display:none;">
          <div align="left"> No Contacto :
            <select name="NO_CONTACTO">
              <option>CONTESTADOR</option>
              <option>FAX</option>
              <option>NO_CONTESTAN</option>
              <option>OTROS_TONOS</option>
              <option>TELEFONO_DANADO</option>
              <option>TELEFONO_DESCONECTADO</option>
              <option>TELEFONO_OCUPADO</option>
              <option>LLAMADA_CORTADA</option>
            </select>
            </p>
            
           
            <input name="tipificar_referido" type="submit" id="tipificar_referido" onclick="MM_validateForm('NOMBRE','','R','APELLIDO','','R','CELULAR','','R','TELCASA','','R');return document.MM_returnValue" value="TIPIFICAR GESTION" />
          </div>
        </div>
        <div id="pAdmin" style="display:none;">
          <div align="left"> Inconsistencias :
            <select name="INCONSISTENCIAS">
              <option>TELEFONO_ERRADO</option>
              <option>VALIDACION_PREVIA_DE_CARTERA</option>
            </select>
            </p>
            
            
            <input name="tipificar_referido" type="submit" id="tipificar_referido" onclick="MM_validateForm('NOMBRE','','R','APELLIDO','','R','CELULAR','','R','TELCASA','','R');return document.MM_returnValue" value="TIPIFICAR GESTION" />
          </div>
        </div></td>
    </tr>
    <tr>
      <td colspan="6">
      <div id="pCall2" style="display:;">
        <div align="left"> No Venta :
          <select name="NO_VENTA" onchange="nventaOnChange(this)">
            <option value="PERMANENCIA_ACTIVA_CON_OTRO_OPERADOR">PERMANENCIA_ACTIVA_CON_OTRO_OPERADOR</option>
            <option value="YA_TIENE_EL _SERVICIO_OFRECIDO">YA_TIENE_EL_SERVICIO_OFRECIDO</option>
            <option value="NO_LE_PARECE_ATRACTIVA_LA_OFERTA">NO_LE_PARECE_ATRACTIVA_LA_OFERTA</option>
            <option value="PROCESOS_DE_SERVICIO">PROCESOS_DE_SERVICIO</option>
            <option value="PENDIENTE_CIERRE_DE_VENTA">PENDIENTE_CIERRE_DE_VENTA</option>
            <option value="CARTERA_MARCACION_PREDICTIVA">CARTERA_MARCACION_PREDICTIVA</option>
            <option value="RECHAZADO_EVIDENTE">RECHAZADO_EVIDENTE</option>
            <option value="CLIENTE_PYME">CLIENTE_PYME</option>
            <option value="NO_TIENE _COMPUTADOR">NO_TIENE _COMPUTADOR</option>
            <option value="SIN_COBERTURA">SIN_COBERTURA</option>
           <!-- <option value="NO_APTO_DATACREDITO">NO_APTO_DATACREDITO</option> -->
          </select>
          <br />
        </div>
      </div>
        <div id="pCall3" style="display:none; text-align: center;">
          <div align="left">
            </p>
            <strong>FORMULARIO DE VENTA :</strong><span style="text-align: left"></span>
            </p>
            <fieldset>
              <legend><strong>Servicios B&aacute;sicos con cargo mensual </strong></legend>
              <strong>Televis&iacute;on :</strong> 
              </p>
              &nbsp;No TV             
              <input name="VENTATV" type="radio" id="radio6" value="Ninguna" checked="checked" />
              &nbsp;&nbsp;&nbsp;&nbsp;UP
              <input type="radio" name="VENTATV" id="radio7" value="UP" />
              &nbsp;&nbsp;&nbsp;&nbsp;VENTA
              <input type="radio" name="VENTATV" id="radio7" value="VENTA" />
              </p>
              &nbsp;Ninguna
              <input name="TV" type="radio" id="radio6" value="Ninguna" checked="checked" />
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B&aacute;sica
              <input type="radio" name="TV" id="radio" value="basica" />
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Digital B&aacute;sica
              <input type="radio" name="TV" id="radio2" value="digital basica" />
              &nbsp;&nbsp;&nbsp;&nbsp;Digital Avanzada
              <input type="radio" name="TV" id="radio3" value="digital avanzada" />
              &nbsp;&nbsp;&nbsp;&nbsp;Digital B&aacute;sica HD
              <input type="radio" name="TV" id="radio4" value="digital basica hd" />
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Digital Avanzada HD
              <input type="radio" name="TV" id="radio5" value="digital avanzada hd" />
              </p>
              <strong>Internet Banda Ancha :</strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <select name="INT_BANDA_ANCHA" class="fuente">
                <option>0</option>
                <option>@1000</option>
                <option>@5000</option>
                <option>@10000</option>
                <option>@20000</option>
                <option>@50000</option>
                <option>@Movil 750Mb</option>
                <option>@Movil 1.5Gb</option>
                <option>@UP5</option>
                <option>@UP10</option>
                <option>@UP20</option>
                <option>@UP50</option>
              </select>
              MB &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; WIFI
              <select name="WIFI" class="fuente">
                <option value="0">NO</option>
                <option value="1">SI</option>
              </select>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>Telefonia Local</strong>
              <select name="TEL" onchange="jpg(this)" class="fuente" id="TEL">
                <option value="0">NO</option>
                <option value="Voz">SI</option>
              </select>
              
              
              <div id="Jpg1" style="display:none;">
        <div align="left"> 
         <!-- <form action="subirarchivo.php?Nick=<?php echo $Nick; ?>" target="inserform" method="post" enctype="multipart/form-data"> -->
              <p><span class="Estilo2"><strong>Cargar Evidente:</strong></span> <br>
                  <input name="cadenatexto" type="text" value="Evidente" size="20" maxlength="100">
                  <input type="hidden" name="MAX_FILE_SIZE" value="100000000">
                  <br>
                <!--  <span class="Estilo2">Enviar un nuevo archivo: </span>  -->
                                   
                  <input name="userfile" type="file" size="15" />
                 <!-- <input name="capacitacion" type="submit" id="capacitacion" value="Enviar"> -->
                <!-- </form> -->
          <br />
        </p>
       </div>
      </div>
              
            </fieldset>
            </p>
            <fieldset>
              <legend><strong>Servicios Adicionales con cargo mensual</strong></legend>
              <strong>Televis&iacute;on :</strong> &nbsp;&nbsp;&nbsp;&nbsp;
              Decodificadores Adicionales&nbsp;&nbsp;&nbsp;&nbsp;
              <input name="DECO" type="text" class="fuente" id="DECO" onclick="value=''" value="" size="5"/>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HD
              <select name="HD" class="fuente" id="HD">
                <option value="0">NO</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
              </select>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PVR
              <select name="PVR" onchange="jpgpvr(this)" class="fuente" id="PVR">
                <option value="0">NO</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
              </select>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HBO
              <select name="HBO" class="fuente" id="HBO">
                <option value="0">NO</option>
                <option value="HBO">SI</option>
              </select>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mini HBO
              <select name="MINI_HBO" class="fuente" id="MINI_HBO">
                <option value="0">NO</option>
                <option value="MINI HBO">SI</option>
              </select>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Movie Pack
              <select name="MOVIE_PACK" class="fuente" id="MOVIE_PACK">
                <option value="0">NO</option>
                <option value="MOVIE PACK">SI</option>
              </select>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mini Movie Pack
              <select name="PACK" class="fuente" id="PACK">
                <option value="0">NO</option>
                <option value="MINI MOVIE PACK">SI</option>
              </select>
              &nbsp;&nbsp;&nbsp;&nbsp;Play Boy
              <select name="PLAYBOY" class="fuente" id="PLAYBOY">
                <option value="0">NO</option>
                <option value="PLAYBOY">SI</option>
              </select>
              &nbsp;&nbsp;&nbsp;&nbsp;Venus
              <select name="VENUS" class="fuente" id="VENUS">
                <option value="0">NO</option>
                <option value="VENUS">SI</option>
              </select>
              &nbsp;&nbsp;&nbsp;&nbsp;Claro Video
              <select name="CLARO_V" class="fuente" id="CLARO_V">
                <option value="0">NO</option>
                <option value="1">SI</option>
              </select>   
              </p>
              <strong>Telefonia Local :</strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              Lineas Adicionales
              <input name="LIN_ADICIONALES" type="text" class="fuente" id="LIN_ADICIONALES" value="" size="5" onclick="value=''"/>
              &nbsp;&nbsp;&nbsp;&nbsp;
              Larga Distancia&nbsp;&nbsp;
              <select name="LARGA_DISTANCIA" onchange="jpgLdistancia(this)" class="fuente" id="LARGA_DISTANCIA">
                <option>0</option>
                <option>60m</option>
                <option>200m</option>
                <option>400m</option>
                <option>800m</option>
                <option>1714m</option>
              </select>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>Otros :</strong> Revista
              <select name="REVISTA" class="fuente" id="REVISTA">
                <option value="0">NO</option>
                <option value="REVISTA">SI</option>
              </select>
              &nbsp;&nbsp;&nbsp;&nbsp;
              
              
              <div id="Jpg1pvr" style="display:none; color: #00F;">
        <div align="left"> 
         <!-- <form action="subirarchivo.php?Nick=<?php echo $Nick; ?>" target="inserform" method="post" enctype="multipart/form-data"> -->
              <p><span class="Estilo2"><strong>Cargar Evidente:</strong></span> <br>
                  <input name="cadenatexto" type="text" value="Evidente" size="20" maxlength="100">
                  <input type="hidden" name="MAX_FILE_SIZE" value="100000000">
                  <br>
                <!--  <span class="Estilo2">Enviar un nuevo archivo: </span>  -->
                                   
                  <input name="userfile" type="file" size="15" />
                 <!-- <input name="capacitacion" type="submit" id="capacitacion" value="Enviar"> -->
                <!-- </form> -->
          <br />
        </p>
</div>
      </div>  
              
            </fieldset>
            </p>
            <fieldset>
              <legend><strong>Datos de Instalaci&oacute;n</strong></legend>
              <p><strong>Asesor:</strong>
                <input name="ASESOR" type="text" class="fuente" id="ASESOR" value="<?php echo($fila2['Nombre']); ?>" size="10" readonly="readonly"/>
                C.C.
                <input name="CC_ASESOR" type="text" class="fuente" id="CC_ASESOR" value="<?php echo $CC_ASESOR;?>" size="10" readonly="readonly"/>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Telefono Grabacion:</strong>
              <input name="TEL_GRABACION" type="text" class="fuente" id="TEL_GRABACION" size="10"/>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
              Por Que Medio Conoce a Claro?
              <select name="COMUNICACIONES" class="fuente" id="FORMATO_HORA3">
                <option value="00010">Volantes</option>
                <option value="00011">Voz a voz</option>
                <option value="00012">Revistas</option>
                <option value="00013">Eventos</option>
                <option value="00014">Televisión</option>
                <option value="00015">Radio</option>
                <option value="00016">Prensa</option>
                <option value="00017">Pagina web Internet</option>
                <option value="00018">Paginas Amarillas</option>
              </select>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              Usted Utiliza :
              <select name="USOLD" class="fuente" id="COMUNICACIONES">
                <option value="LDN_">Utiliza Larga Distancia Nacional</option>
                <option value="LDI_">Utiliza Larga Distancia Internacional</option>
                <option value="LDNI">Utiliza Larga Distancia Nacional e Internacional</option>
                <option value="NLD_">NO Utilizada Larga Distancia</option>
              </select>
              </p>
              Fecha Instalacion :
              <!-- <input name="FECHA_ENTREGA" type="text" class="fuente" id="FECHA_ENTREGA" value="AAAA-MM-DD" size="10" onClick="value=''"/> -->
              <input name="FECHA_ENTREGA" type="text" class="tcal" id="FECHA_ENTREGA" value="" size="10" readonly="readonly" />
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;             
              Franja Horaria :
              <select name="FORMATO_HORA" class="fuente" id="FORMATO_HORA">
                <option>AM</option>
                <option>PM</option>
              </select>
              
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;             
                  Recomendaciones : 
                   <textarea name="RECOMENDADIONES" cols="20" class="fuente" id="RECOMENDACIONES"></textarea>
              
              </p>
              Direcci&oacute;n De Instalaci&oacute;n :
              <input name="DIRECCION_INSTALACION" type="text" class="fuente" id="DIRECCION_INSTALACION" size="50" value=""/>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E Mail :
              <input name="E_MAIL" type="text" class="fuente" id="E_MAIL" size="30" value=""/>
              <span class="nota">Nota: Si el cliente no tiene Email dejar el campo en blanco.</span>
</P>
              <input name="enviaformventaclinuevo" type="submit" id="enviaformventaclinuevo" onclick="MM_validateForm('NUMERO_CC','','R','NOMBRE','','R','APELLIDO','','R','CELULAR','','R','TELCASA','','R','DIRECCION','','R','CIUDAD','','R','ESTRATO','','R','TEL_GRABACION','','RisNum','FECHA_ENTREGA','','R','DIRECCION_INSTALACION','','R','E_MAIL','','NisEmail');return document.MM_returnValue" value="ACTUALIZAR VENTA" />
              </p>
            </fieldset>
            <!-- <tr bgcolor="#CA0000">
          <td colspan="4"><strong class="subtitulo">Tipificacion</strong></td>
            </tr>     -->
          </div>
        </div></td>
    </tr>
    <tr>
      <td colspan="6" align="right"><div id="pNventa" style="display:;">
        <div align="left"> Permanencia Otro Operador :
          <select name="PERMANENCIA">
            <option>ETB</option>
            <option>UNE</option>
            <option>TELEFONICA</option>
            <option>DIRECT_TV</option>
            <option>SUPER_CABLE</option>
          </select>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          Fecha Permanencia :
          <input name="FECHA_PERMANENCIA" type="text" class="tcal" id="FECHA_PERMANENCIA" value="" size="10" readonly="readonly" />
          <!-- <input name="FECHA_PERMANENCIA" type="text" class="fuente" id="FECHA_PERMANENCIA" size="10"/> -->
          </p>          
          
          <input name="tipificar_referido" type="submit" id="enviaformventa2" onclick="MM_validateForm('NUMERO_CC','','NisNum','NOMBRE','','R','APELLIDO','','R','CELULAR','','RisNum','TELCASA','','RisNum','ESTRATO','','NisNum','FECHA_PERMANENCIA','','R');return document.MM_returnValue" value="TIPIFICAR GESTION" />
          <br />
        </div>
      </div>
        <div id="pNventa2" style="display:none;">
          <div align="left">
            </p>
                        
            
            <input name="tipificar_referido" type="submit" id="tipificar_referido" onclick="MM_validateForm('NOMBRE','','R','APELLIDO','','R','CELULAR','','R','TELCASA','','R');return document.MM_returnValue" value="TIPIFICAR GESTION" />
            <br />
          </div>
        </div>
        <div id="pNventa3" style="display:none;">
          <div align="left"> No le parece atractiva la oferta :
            <select name="NO_OFERTA">
              <option>NO_DESEA_PERMANENCIA</option>
              <option>PRECIO</option>
            </select>
            </p>
                        
            
            <input name="tipificar_referido" type="submit" id="tipificar_referido" onclick="MM_validateForm('NOMBRE','','R','APELLIDO','','R','CELULAR','','R','TELCASA','','R');return document.MM_returnValue" value="TIPIFICAR GESTION" />
          </div>
        </div>
        <div id="pNventa4" style="display:none;">
          <div align="left"> Procesos de Servicio :
            <select name="PROCESOS_SERVICIO">
              <option>PROBLEMAS_TECNICOS_SERVICIO_CLARO</option>
            </select>
            </p>
                        
            
            <input name="tipificar_referido" type="submit" id="tipificar_referido" onclick="MM_validateForm('NOMBRE','','R','APELLIDO','','R','CELULAR','','R','TELCASA','','R');return document.MM_returnValue" value="TIPIFICAR GESTION" />
          </div>
        </div>
        <div id="pNventa5" style="display:none;">
          <div align="left"> Pendiente Cierre de Venta :
            <select name="PENDIENTE_CIERRE_V">
              <option>PENDIENTE_ENVIO_SOPORTE</option>
              <option>PENDIENTE_EVALUACION_EVIDENTE</option>
              <option>PENDIENTE_DESCONEXION_COMPETENCIA</option>
              <option>PENDIENTE_CREACION_DIRECCION</option>
              <option>PENDIENTE_CAMBIO_ESTRATO</option>
              <option>PENDIENTE_CESION_DE_CONTRATO</option>
            </select>
            </p>
                        
            
            <input name="tipificar_referido" type="submit" id="tipificar_referido" onclick="MM_validateForm('NOMBRE','','R','APELLIDO','','R','CELULAR','','R','TELCASA','','R');return document.MM_returnValue" value="TIPIFICAR GESTION" />
          </div>
        </div></td>
    </tr>
    <tr>
      <td colspan="6" align="right">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="6" align="center"><!-- <input type="submit" name="enviaformventa" id="enviaformventa" value="ACTUALIZAR" />    --></td>
    </tr>
  </form>
</table>
<?php 
}
 ?>

<script type="text/javascript">
swfobject.registerObject("FlashID");
</script>
</body>
</html>