<?php
require_once('jpgraph-3.5.0b1/src/jpgraph.php');

require_once('jpgraph-3.5.0b1/src/jpgraph_bar.php');

mysqli_connect("localhost","root","");
mysqli_select_db("inventario");

$sql="select * from ventas";
$res=mysqli_query($sql);

while($row=mysqli_fetch_array($res))
{
	$datos[]=$row['cantidad'];
	$labels[]=$row['producto'];
}
//formato general
$grafico=new Graph(600,500,'auto');
$grafico->SetScale("textint");
$grafico->title->set("EJEMPLO GRAFICA");
$grafico->xaxis->title->set("PRODUCTOS");
$grafico->xaxis->SetTickLabels($labels);
$grafico->yaxis->title->set("ventas");

$barplot1=new BarPlot($datos);

$barplot1->SetFillGradient("#14BAF1","#F19914", GRAD_HOR);

$barplot1->SetWidth(40);

$grafico->Add($barplot1);

$grafico->Stroke();

$grafico->Stroke("IMG3.JPEG");


?>