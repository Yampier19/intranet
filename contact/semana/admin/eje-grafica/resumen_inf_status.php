<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
<style type="text/css">
.nombre2 {
	font-size: 18px;
	font-weight: bold;
	color: #FFF;
	font-family: "Trebuchet MS", cursive;
	background-color: #3CC;
	border-radius:10px;
	background:#33CCCC;
	border:0px;	/* background-color: #69F */
	text-align: left;
}

.contacordeon {
	font-size: 18px;
	font-weight: bold;
	color: #000;
	font-family: "Trebuchet MS", cursive;
	background-color: #FFF;
	/*border-radius:10px;*/
	border:0px;	/* background-color: #69F */
	text-align: left;
}


.Estilo1 {
	font-size: 20px;
	text-transform:uppercase;
	font-family: "Trebuchet MS", cursive;
}

.Estilo2 {
	font-size: 60%;
	text-transform:uppercase;
	font-family: "Trebuchet MS", cursive;
	color: #000;	
}

.tabla {	
	font-family: "Trebuchet MS", cursive;
	font-size: 12px;
	color: #FFF;
}
.NOTA {
	font-size: 11px;
	text-transform:uppercase;		
}
.espacio {
	font-size: 5px;
	text-transform:uppercase;		
}
.Estilo2 tr td {
	color: #191970;
	
}
#form1 .nombre2 tr td div strong {
	color: #000;
}
input
{
	font-family: "Trebuchet MS", cursive;
	font-size:100%;
	width:auto;
	height:auto;
}
select
{
	font-family: "Trebuchet MS", cursive;
	font-size:100%;
	width:auto;
	height:auto;
}
td
{
	width:auto;
}
</style>
</head>
<body>
<?PHP
////session_start();
mysqli_connect("localhost","root","");
mysqli_select_db("inventario");
   
   $total=mysqli_query($conex,"SELECT COUNT(ID) AS 'TOTAL',`producto`,`cantidad` FROM ventas");
   $activos=mysqli_query($conex,"SELECT COUNT(ID) AS 'ACTIVOS' FROM pacientes WHERE STATUS='ACTIVO' AND FECHA_ULTIMA_GESTION>='".$FECHA_INI."' AND FECHA_ULTIMA_GESTION<='".$FECHA_FIN."' ");
   $suspendidos=mysqli_query($conex,"SELECT COUNT(ID) AS 'SUSPENDIDOS' FROM pacientes  WHERE STATUS='SUSPENDIDOS' OR STATUS='SUSPENDIDO' AND FECHA_ULTIMA_GESTION>='".$FECHA_INI."' AND FECHA_ULTIMA_GESTION<='".$FECHA_FIN."'");
   $retirados=mysqli_query($conex,"SELECT COUNT(ID) AS 'RETIRADOS' FROM pacientes WHERE STATUS='RETIRADO' AND FECHA_ULTIMA_GESTION>='".$FECHA_INI."' AND FECHA_ULTIMA_GESTION<='".$FECHA_FIN."'");
 
	  echo mysqli_error($conex);
	  
	  while($fila=mysqli_fetch_array($total))
	  {
		  $TOTAL=$fila['TOTAL'];
	  }
	  	  while($fila1=mysqli_fetch_array($activos))
	  {
		  $ACTIVOS=$fila1['ACTIVOS'];
	  }
	  	  while($fila2=mysqli_fetch_array($suspendidos))
	  {
		  $SUSPENDIDOS=$fila2['SUSPENDIDOS'];
	  }
	  	  while($fila2=mysqli_fetch_array($retirados))
	  {
		  $RETIRADOS=$fila2['RETIRADOS'];
	  }
	  
  
?>
<table width="70%" border="0" align="left" style="border:#255398 3px solid;border-bottom-right-radius: 10px; border-bottom-left-radius: 10px;border-top-left-radius: 10px; border-top-right-radius: 10px;" >

  <tr>
    <td  bgcolor="#33CCCC" align="center" width="100%" height="20" colspan="3" style="border-top-left-radius: 10px;
border-top-right-radius: 10px; font-size:100%;">&nbsp;&nbsp;&nbsp;RESUMEN INFORMES</td>
  </tr>
      <tr  bgcolor="#33CCCC" align="center">
     <td class="botones"><strong>STATUS</strong></td>
     <td width="100" bgcolor="#33CCCC"><strong>CANTIDAD REGISTROS</strong></td>              
     <td class="botones"><strong>PORCENTAJE</strong></td>  
  </tr>
      <tr>
          <td  bgcolor="#33CCCC">ACTIVOS :</td>
          <td bgcolor="#FFFFFF"><strong>
            <input name="GESTIONADAS" type="text" id="GESTIONADAS" onchange = "this.form.submit()" value = "<?php echo $ACTIVOS; ?>" readonly="readonly"/>
          </strong></td>
          <td bgcolor="#FFFFFF"><strong>
          <?php $ACTIVOS=($ACTIVOS*100)/$TOTAL ?>
            <input name="GESTIONADAS_P" type="text" id="GESTIONADAS_P" onchange = "this.form.submit()" value = "<?php echo round($ACTIVOS, 2); ?>%" readonly="readonly"/>
          </strong></td>
      </tr>
              <tr>
          <td  bgcolor="#33CCCC" >SUSPENDIDOS :</td>
          <td bgcolor="#FFFFFF"><strong>
            <input name="PROGRAMADAS" type="text" id="PROGRAMADAS" onchange = "this.form.submit()" value = "<?php echo $SUSPENDIDOS; ?>" readonly="readonly"/>
          </strong></td>
          <td bgcolor="#FFFFFF"><strong>
          <?php $SUSPENDIDOS=($SUSPENDIDOS*100)/$TOTAL ?>
            <input name="PROGRAMADAS_P" type="text" id="PROGRAMADAS_P" onchange = "this.form.submit()" value = "<?php echo round($SUSPENDIDOS, 2) ; ?>%" readonly="readonly"/>
          </strong></td>
          </tr>
              <tr>
          <td  bgcolor="#33CCCC">RETIRADOS :</td>
          <td bgcolor="#FFFFFF"><strong>
            <input name="RETIRADOS" type="text" id="RETIRADOS" onchange = "this.form.submit()" value = "<?php echo $RETIRADOS; ?>" readonly="readonly"/>
          </strong></td>
          <td bgcolor="#FFFFFF"><strong>
          <?php $RETIRADOS=($RETIRADOS*100)/$TOTAL ?>
            <input name="RETIRADOS_P" type="text" id="RETIRADOS_P" onchange = "this.form.submit()" value = "<?php echo round($RETIRADOS, 2); ?>%" readonly="readonly"/>
          </strong></td>
          </tr>
              <tr>
          <td  bgcolor="#33CCCC">TOTAL :</td>
          <td bgcolor="#FFFFFF"><strong>
            <input name="TOTAL" type="text" id="TOTAL" onchange = "this.form.submit()" value = "<?php echo $TOTAL; ?>" readonly="readonly"/>
          </strong></td>
           <td bgcolor="#FFFFFF"><strong>
            <input name="TOTAL_P" type="text" id="TOTAL-P" onchange = "this.form.submit()" value = "100%" readonly="readonly"/>
          </strong></td>
          
          </tr>
                      <tr bgcolor="#33CCCC" height="10">
              <td colspan="3" class="botones" style="border-bottom-right-radius: 10px;
border-bottom-left-radius: 10px;">

            </td>  
              </tr>
              <tr>
              <td>
              <a href="ver.php">GRAFICAR</a>
              </td>
              </tr>
  </table>
</body>
</html>